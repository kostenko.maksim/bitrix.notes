<?php

$MESS["SBERBANK_PAYMENT_DESCRIPTION"] = 'Ответ платежной системы';
$MESS["SBERBANK_PAYMENT_ERROR_TITLE"] = 'Ошибка №';
$MESS["SBERBANK_PAYMENT_MESSAGE_PAYMENT_ALLREADY"] = 'Ваш заказ уже оплачен!';
$MESS["SBERBANK_PAYMENT_PAYMENT_TITLE"] = 'Сумма к оплате по счету';
$MESS["SBERBANK_PAYMENT_PAYMENT_BUTTON_NAME"] = 'Оплатить';
$MESS["SBERBANK_PAYMENT_PAYMENT_DESCRIPTION"] = 'Вы будете перенаправлены на страницу оплаты';
$MESS["SBERBANK_PAYMENT_ERROR_MESSAGE_UNDEFIND"] = 'Возникла неопознанная ошибка, просьба обратиться к администрации сайта';
$MESS["SBERBANK_PAYMENT_FOOTER_DESCRIPTION"] = 'При отказе от покупки, для возврата денежных средств необходимо обращаться в магазин.';
$MESS["SBERBANK_PAYMENT_FOOTER_DESCRIPTION_2"] = 'Уважаемый покупатель! Если в заказе присутствует весовой товар на вашей карте будет зарезервирована сумма на 5% больше, чем сумма вашего заказа. Точная сумма заказа будет списана после сборки заказа. Остаток денежных средств будет разблокирован.';
$MESS['SBERBANK_PAYMENT_FOOTER_DESCRIPTION_3'] = 'Обращаем внимание, что без оплаты ваш заказ не может быть зарезервирован и отправлен вам.';
