<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;


Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/include/sale_payment/sberbank_ecom2/lang/ru/template/payment.php');
?>
<div class="sberbank__wrapper">
    <div class="sberbank__content">
        <?php if (in_array($params['sberbank_result']['errorCode'], array(999, 1, 2, 3, 4, 5, 7, 8))) { ?>
            <span class="sberbank__description"><?= Loc::getMessage("SBERBANK_PAYMENT_DESCRIPTION"); ?>:</span>
            <span class="sberbank__error-code"><?= Loc::getMessage("SBERBANK_PAYMENT_ERROR_TITLE"); ?><?= $params['sberbank_result']['errorCode'] ?></span>
            <span class="sberbank__error-message"><?= $params['sberbank_result']['errorMessage'] ?></span>

        <?php } else if ($params['sberbank_result']['payment'] == 1) { ?>

            <span class="sberbank__error-message"><?= getMessage("SBERBANK_PAYMENT_MESSAGE_PAYMENT_ALLREADY"); ?></span>

        <?php } else if ($params['sberbank_result']['errorCode'] == 0) { ?>

        <? /*if ($params['SBERBANK_HANDLER_AUTO_REDIRECT'] == 'Y') { ?>
            <script>window.location = '<?=$params['payment_link']?>';</script>
        <?php }*/ ?>

            <span class="sberbank__price-string"><?= Loc::getMessage("SBERBANK_PAYMENT_PAYMENT_TITLE"); ?>
                : <b><?php
                    $paySum = SberbankBitrixHelper::getTotalOrderSumCoeff($params['SBERBANK_ORDER_NUMBER']); //+5% для заказов с весовым товаром
                    //$paySum = $params['SBERBANK_ORDER_AMOUNT'];
                    echo CurrencyFormat($paySum, $params['currency'])
                    ?></b></span>
            <a href="<?= $params['payment_link'] ?>"
               class="sberbank__payment-link js-sber-link"><?= Loc::getMessage("SBERBANK_PAYMENT_PAYMENT_BUTTON_NAME"); ?></a>
            <span class="sberbank__payment-description"><?= Loc::getMessage("SBERBANK_PAYMENT_PAYMENT_DESCRIPTION"); ?></span>

            <script>
                document.addEventListener("DOMContentLoaded", function() {
                    $('.js-sber-link').on('click', function(e) {
                        e.preventDefault();

                        $.fancybox.open({
                            src  : '<?=$params["payment_link"];?>',
                            type : 'iframe',
                            opts : {
                                afterShow : function( instance, current ) {

                                },
                                afterLoad  : function(instance, current) {
                                    let iframeId = '#' + current.$iframe[0].id;
                                    let curCnt = current;

                                    let iframeHead = $(iframeId).contents().find("head");
                                    let iframeCSS = "<style>#root, body, html{overflow: hidden !important;}</style>";
                                    $(iframeHead).append(iframeCSS);

                                    //console.log(iframeId);
                                    //console.log($(iframeId));

                                    $(iframeId).on('load', function(e) {
                                        if ($(window).width() > 768) {
                                            curCnt.width = 500;
                                            curCnt.height = 800;
                                        } else {
                                            current.width = '100%';
                                            current.height = '100%';
                                        }

                                        let iframeHead = $(iframeId).contents().find("head");
                                        let iframeCSS = "<style>#root, body, html{overflow: hidden !important;}</style>";
                                        $(iframeHead).append(iframeCSS);

                                        //console.dir($(iframeId).get(0).contentWindow.location.href);

                                        let redirectLink = '';

                                        try {
                                            if(typeof $(iframeId).get(0).contentWindow === 'object') {
                                                if (typeof $(iframeId).get(0).contentWindow.location === 'object') {
                                                    if ($(iframeId).get(0).contentWindow.location.href !== '') {
                                                        redirectLink = $(iframeId).get(0).contentWindow.location.href;
                                                    }
                                                }
                                            }
                                        } catch (e) {}

                                        if (redirectLink !== '') {
                                            if (redirectLink.match(/bitrix\/tools\/sale_ps_result\.php/)) {
                                                //$.fancybox.close();
                                                //window.location.href = $(iframeId).get(0).contentWindow.location.href;
                                                window.location.href = '/personal/order_detail/?id=<?=(int)$_GET['ORDER_ID'];?>'
                                            }
                                        }
                                    });

                                    if ($(window).width() > 768) {
                                        current.width = 500;
                                        current.height = 800;
                                    } else {
                                        current.width = '100%';
                                        current.height = '100%';
                                    }
                                }
                            },
                        });
                    });

                    $('.js-sber-link').trigger('click');
                });
            </script>

        <?php } else { ?>
            <span class="sberbank__error-message"><?= Loc::getMessage("SBERBANK_PAYMENT_ERROR_MESSAGE_UNDEFIND"); ?></span>

        <?php } ?>
    </div>
    <div class="sberbank__footer">
        <p><span class="sberbank__description"><?// = Loc::getMessage("SBERBANK_PAYMENT_FOOTER_DESCRIPTION_2"); ?></span>
        </p>
        <p><span class="sberbank__description"><?= Loc::getMessage("SBERBANK_PAYMENT_FOOTER_DESCRIPTION"); ?></span></p>
        <p><span class="sberbank__description"><b><?= Loc::getMessage("SBERBANK_PAYMENT_FOOTER_DESCRIPTION_3"); ?></b></span></p>
    </div>
</div>

<style>
    .sberbank__wrapper {
        font-family: arial;
        text-align: left;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    .sberbank__price-block {
        font-family: arial;
        display: block;
        margin: 20px 0px;
    }

    .sberbank__price-string {
        font-family: arial;
        font-weight: bold;
        font-size: 14px;
    }

    .sberbank__price-string b {
        font-family: arial;
        font-size: 20px;
    }

    .sberbank__content {
        font-family: arial;
        width: 400px;
        max-width: 100%;
        padding: 10px 10px 13px;
        border: 1px solid #e5e5e5;
        text-align: center;
        margin-bottom: 12px;
    }

    .sberbank__payment-link {
        font-family: arial;
        display: inline-block;
        width: 320px;
        max-width: 100%;
        margin: 8px 0 5px;
        background-color: #1eb42f;
        color: #FFF;
        border: none;
        box-shadow: none;
        outline: none;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        padding: 6px 12px;
        text-decoration: none;
    }

    .sberbank__payment-link:hover, .sberbank__payment-link:active, .sberbank__payment-link:focus {
        font-family: arial;
        background: #189d27;
        color: #fff;
    }

    .sberbank__payment-description {
        font-family: arial;
        display: block;
        font-size: 12px;
        color: #939393;
    }

    .sberbank__description {
        font-family: arial;
        font-size: 12px;
        max-width: 400px;
        display: block;
    }

    .sberbank__error-code {
        font-family: arial;
        color: red;
        font-size: 20px;
        display: block;
        margin-top: 5px;
        margin-bottom: 7px;
    }

    .sberbank__error-message {
        font-family: arial;
        color: #000;
        font-size: 14px;
        display: block;
    }

    .fancybox-is-open .fancybox-bg {
        opacity: 0.5 !important;
    }

    .fancybox-iframe {
        border-radius: 8px !important;
        padding: 10px 8px 0 !important;
        background: #f1f2f2 !important;
    }

    .fancybox-slide.fancybox-slide--iframe {
        border-radius: 8px !important;
    }

    .fancybox-slide--iframe .fancybox-content {
        border-radius: 8px !important;
        max-width: 500px !important;
        max-height: 800px !important;
        width: 500px !important;
        height: 800px !important;
    }

    @media (max-width: 768px) {
        .fancybox-slide--iframe .fancybox-content {
            border-radius: 8px !important;
            max-width: 100% !important;
            max-height: 100% !important;
            width: 100% !important;
            height: 100% !important;
        }

        .fancybox-slide {
            padding: 0 !important;
        }
    }
</style>
