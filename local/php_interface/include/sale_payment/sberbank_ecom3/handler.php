<?php 

namespace Sale\Handlers\PaySystem;

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Request;
use Bitrix\Main\Text\Encoding;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web;
use Bitrix\Sale\BusinessValue;
use Bitrix\Sale\PaymentCollection;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\Payment;
use Bitrix\Sale\Order;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Sale\PaySystem\Service;


IncludeModuleLangFile(__FILE__);
if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/sberbank.ecom3/config.php')) {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/sberbank.ecom3/config.php');
}

Loader::includeModule('sberbank.ecom3');

/**
 * Class sberbank_ecom3Handler
 * копи-паст обработчика /bitrix/modules/sberbank.ecom3/handler/handler.php
 * создан чтобы добавлять свои доработки
 *
 * @see  /bitrix/modules/sberbank.ecom3/handler/handler.php
 */

class sberbank_ecom3Handler extends PaySystem\ServiceHandler
    implements PaySystem\IPrePayable, PaySystem\IRefundExtended, PaySystem\IHold, PaySystem\ICheckable
{
    /**
     * ID свойств заказа
     * В него записывается ID транзакции
     */
    protected $arPropsIds = [
        'PAYSYSTEM_ID' => [
            1 => '55',
            2 => '114',
            3 => '190'
        ]
    ];

    /**
     * @param Payment $payment
     * @param Request|null $request
     * @return PaySystem\ServiceResult
     */
    public function initiatePay(Payment $payment, Request $request = null)
    {
        $moduleId = 'sberbank.ecom3';

        $gateway = new \Sberbank\Payments\Gateway;

        // module settings
        $gateway->setOptions(array(
            'module_id' => Option::get($moduleId, 'MODULE_ID'),
            'gate_url_prod' => Option::get($moduleId, 'SBERBANK_PROD_URL'),
            'gate_url_test' => Option::get($moduleId, 'SBERBANK_TEST_URL'),
            'module_version' => Option::get($moduleId, 'MODULE_VERSION'),
            'iso' => unserialize(Option::get($moduleId, 'ISO')),
            'cms_version' => 'Bitrix ' . SM_VERSION,
            'language' => 'ru',
            'default_cartItem_tax' => Option::get($moduleId, 'TAX_DEFAULT'),
            'ignore_product_tax' => Option::get($moduleId, 'IGNORE_PRODUCT_TAX'),
            'callback_mode' => Option::get($moduleId, 'RBS_ENABLE_CALLBACK'),
            'measurement_code' => Option::get($moduleId, 'MEASUREMENT_CODE'),
            // 'additionalOfdParams' => array(
            // 'agent_info.type' => 6,
            // 'agent_info.paying.operation' => '',
            // 'agent_info.paying.phones' => '',
            // 'agent_info.paymentsOperator.phones' => '',
            // 'agent_info.MTOperator.address' => '',
            // 'agent_info.MTOperator.inn' => '',
            // 'agent_info.MTOperator.name' => '',
            // 'agent_info.MTOperator.phones' => '',
            // 'supplier_info.inn' => '',
            // 'supplier_info.name' => '',
            // 'supplier_info.phones' => '',
            // 'cashier' => '',
            // 'additional_check_props' => '',
            // 'additional_user_props.name' => '',
            // 'additional_user_props.value' => '',
            // ),
        ));

        // handler settings
        $gateway->setOptions(array(
            'ofd_tax' => $this->getBusinessValue($payment,
                'SBERBANK_OFD_TAX_SYSTEM') == 0 ? 0 : $this->getBusinessValue($payment, 'SBERBANK_OFD_TAX_SYSTEM'),
            'ofd_enabled' => $this->getBusinessValue($payment, 'SBERBANK_OFD_RECIEPT') == 'Y' ? 1 : 0,
            'ffd_version' => $this->getBusinessValue($payment, 'SBERBANK_FFD_VERSION'),
            'ffd_payment_object' => $this->getBusinessValue($payment, 'SBERBANK_FFD_PAYMENT_OBJECT'),
            'ffd_payment_object_delivery' => $this->getBusinessValue($payment, 'SBERBANK_FFD_PAYMENT_OBJECT_DELIVERY'),
            'ffd_payment_method' => $this->getBusinessValue($payment, 'SBERBANK_FFD_PAYMENT_METHOD'),
            'ffd_payment_method_delivery' => $this->getBusinessValue($payment, 'SBERBANK_FFD_PAYMENT_METHOD_DELIVERY'),
            'test_mode' => $this->getBusinessValue($payment, 'SBERBANK_GATE_TEST_MODE') == 'Y' ? 1 : 0,
            'handler_two_stage' => $this->getBusinessValue($payment, 'SBERBANK_HANDLER_TWO_STAGE') == 'Y' ? 1 : 0,
            'handler_logging' => $this->getBusinessValue($payment, 'SBERBANK_HANDLER_LOGGING') == 'Y' ? 1 : 0,
            'enable_cacert' => $this->getBusinessValue($payment, 'SBERBANK_API_ENABLE_CACERT') == 'Y' ? true : false,
        ));

        $orderId = $payment->getField('ORDER_ID');

        // своя доработка - холдирование
        $amount = $this->getTotalOrderSumCoeff($orderId);; // Общая цена с учетом 5%
        if (empty($amount)) {
            $amount = $this->getBusinessValue($payment, 'SBERBANK_ORDER_AMOUNT');
        }

        $gateway->buildData(array(
            'orderNumber' => $this->getBusinessValue($payment,
                    'SBERBANK_ORDER_NUMBER') . '_' . $payment->getField('ID'),
            'amount' => $amount,
            'userName' => $this->getBusinessValue($payment, 'SBERBANK_GATE_LOGIN'),
            'password' => $this->getBusinessValue($payment, 'SBERBANK_GATE_PASSWORD'),
            'description' => $this->getOrderDescription($payment, 'SBERBANK_ORDER_DESCRIPTION'),
            'currency' => "643",
            'language' => 'ru',

        ));

        //$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== "off" ? 'https://' : 'http://';
        $protocol = 'https://';
        $domain_name = strtok($_SERVER['HTTP_HOST'], ":");

        if (strlen($domain_name) < 3) {
            $domain_name = $_SERVER['SERVER_NAME'];
        }
        if (strlen($domain_name) < 3) {
            $domain_name = Option::get($moduleId, 'NOTIFY_URL', '');
        }

        if (strlen($domain_name) > 3) {
            $gateway->setOptions(
                array(
                    'domain_found' => true,
                    'callback_url' => html_entity_decode($protocol . $domain_name . '/bitrix/tools/sale_ps_result.php?PAYMENT=SBERBANK&OPERATION_CALLBACK=SBER')
                )
            );
        }

        $gateway->buildData(array(
            'returnUrl' => $protocol . $domain_name . '/bitrix/tools/sale_ps_result.php' . '?PAYMENT=SBERBANK&ORDER_ID=' . $payment->getField('ORDER_ID') . '&PAYMENT_ID=' . $payment->getField('ID'),
            'failUrl' => $protocol . $domain_name . '/bitrix/tools/sale_ps_result.php' . '?PAYMENT=SBERBANK&ORDER_ID=' . $payment->getField('ORDER_ID') . '&PAYMENT_ID=' . $payment->getField('ID')
        ));

        $Order = Order::load($payment->getOrderId());
        $propertyCollection = $Order->getPropertyCollection();

        $phone_key = strlen(Option::get($moduleId, 'OPTION_PHONE')) > 0 ? Option::get($moduleId,
            'OPTION_PHONE') : 'PHONE';
        $email_key = strlen(Option::get($moduleId, 'OPTION_EMAIL')) > 0 ? Option::get($moduleId,
            'OPTION_EMAIL') : 'EMAIL';
        $fio_key = strlen(Option::get($moduleId, 'OPTION_FIO')) > 0 ? Option::get($moduleId, 'OPTION_FIO') : 'FIO';

        $phone = preg_replace('/\D+/', '', $this->getPropertyValueByCode($propertyCollection, $phone_key));

        if (substr($phone, 0, 1) == '7') {
            $phone = '+' . $phone;
        }
        if (substr($phone, 0, 1) == '8') {
            $phone[0] = '7';
            $phone = '+' . $phone;
        }

        $gateway->setOptions(array(
            'customer_name' => $this->getPropertyValueByCode($propertyCollection, $fio_key),
            'customer_email' => $this->getPropertyValueByCode($propertyCollection, $email_key),
            'customer_phone' => $phone,
        ));

        if ($gateway->ofdEnable()) {
            $Basket = $Order->getBasket();
            $basketItems = $Basket->getBasketItems();
            $positions = [];
            $lastIndex = 0;
            foreach ($basketItems as $key => $BasketItem) {
                $lastIndex = $key + 1;
                $position = array(
                    'positionId' => strval($key),
                    'itemCode' => strval($BasketItem->getProductId()),
                    'name' => str_replace("\n", "", mb_substr($BasketItem->getField('NAME'), 0, 120)),
                    'itemAmount' => $BasketItem->getFinalPrice(),
                    'itemPrice' => $BasketItem->getPrice(),
                    'quantity' => array(
                        'value' => $BasketItem->getQuantity(),
                        'measure' => $BasketItem->getField('MEASURE_NAME') ? $BasketItem->getField('MEASURE_NAME') : Loc::getMessage('SBERBANK_PAYMENT_FIELD_MEASURE'),
                    ),
                    'paymentMethod' => 'full_payment',
                    'paymentObject' => "commodity",
                    'tax' => array(
                        'taxType' => $gateway->getTaxCode($BasketItem->getField('VAT_RATE') * 100),
                    ),
                );
                // If need support suplier_info
                // $position['agent_info'] = array(
                // 	'agent_info.type' => '6',
                // 	'agent_info.MTOperator.phones' => '+79169998877',
                // );

                $positions[] = $position;
                // $gateway->setPosition($position);
            }

            if ($Order->getField('PRICE_DELIVERY') > 0) {
                Loader::includeModule('catalog');
                $deliveryInfo = \Bitrix\Sale\Delivery\Services\Manager::getById($Order->getField('DELIVERY_ID'));

                $deliveryVatItem = \CCatalogVat::GetByID($deliveryInfo['VAT_ID'])->Fetch();
                $gateway->setOptions(array(
                    'delivery' => true,
                ));
                $positions[] = array(
                    'positionId' => strval($lastIndex + 1),
                    'itemCode' => 'DELIVERY_' . $Order->getField('DELIVERY_ID'),
                    'name' => Loc::getMessage('SBERBANK_PAYMENT_FIRLD_DELIVERY'),
                    'itemAmount' => $Order->getField('PRICE_DELIVERY'),
                    'itemPrice' => $Order->getField('PRICE_DELIVERY'),
                    'quantity' => array(
                        'value' => 1,
                        'measure' => Loc::getMessage('SBERBANK_PAYMENT_FIELD_MEASURE'),
                    ),
                    'paymentMethod' => 'full_payment',
                    'paymentObject' => "service",
                    'tax' => array(
                        'taxType' => $gateway->getTaxCodeDelivery($deliveryVatItem['RATE']),
                    ),
                );
            }

            if (Option::get($moduleId, 'DISCOUNT_HELPER')) {
                // DISCOUNT CALCULATE
                $RBS_Discount = new \Sberbank\Payments\Discount;
                $result_order_sum = $Order->getPrice() - $Order->getSumPaid();
                $discount = $RBS_Discount->discoverDiscount($result_order_sum, $positions);
                if ($discount > 0) {
                    $RBS_Discount->setOrderDiscount($discount);
                    $recalculatedPositions = $RBS_Discount->normalizeItems($positions);
                    $recalculatedAmount = $RBS_Discount->getResultAmount();
                    $positions = $recalculatedPositions;
                }
            }

            foreach ($positions as $key => $position) {
                $gateway->setPosition($position);
            }
        }

        $gateResponse = $gateway->registerOrder();
        $params = array(
            'sberbank_result' => $gateResponse,
            'payment_link' => $gateway->getPaymentLink(),
            'currency' => $payment->getField('CURRENCY'),
            'auto_redirect_exceptions' => unserialize(Option::get($moduleId, 'AUTO_REDIRECT_EXCEPTIONS')),
        );
        $this->setExtraParams($params);

        $result = $this->showTemplate($payment, "payment");
        if (method_exists($result, 'setPaymentUrl')) {
            $result->setPaymentUrl($gateway->getPaymentLink());
        }
        return $result;
    }

    public function processRequest(Payment $payment, Request $request)
    {
        global $APPLICATION;
        $moduleId = 'sberbank.ecom3';
        $result = new PaySystem\ServiceResult();
        $action = $request->get('action');

        if ($action == 'cancel') {
            return $this->cancel($payment);
        }

        $handler_logging = $this->getBusinessValue($payment, 'SBERBANK_HANDLER_LOGGING') == 'Y' ? 1 : 0;

        $gateway = new \Sberbank\Payments\Gateway;
        $gateway->setOptions(array(
            // module settings
            'gate_url_prod' => Option::get($moduleId, 'SBERBANK_PROD_URL'),
            'gate_url_test' => Option::get($moduleId, 'SBERBANK_TEST_URL'),
            'test_mode' => $this->getBusinessValue($payment, 'SBERBANK_GATE_TEST_MODE') == 'Y' ? 1 : 0,
            'callback_redirect' => $request->get('CALLBACK_REDIRECT') == 1 ? 1 : 0,
            'handler_logging' => $request->get('operation') === 'declinedByTimeout' ? false : $handler_logging
        ));

        $data = array(
            'userName' => $this->getBusinessValue($payment, 'SBERBANK_GATE_LOGIN'),
            'password' => $this->getBusinessValue($payment, 'SBERBANK_GATE_PASSWORD'),
            'orderId' => $request->get('mdOrder'),
            'orderNumber' => $request->get('ORDER_ID'),
        );

        $gateway->buildData($data);

        $gateResponse = $gateway->checkOrder();

        $resultId = explode("_", $gateResponse['orderNumber']);
        array_pop($resultId);
        $res_payment_id = array_pop($resultId);
        $resultId = implode('_', $resultId);

        $successPayment = true;
        $change_order_status_logic = true;

        if ($resultId != $this->getBusinessValue($payment, 'SBERBANK_ORDER_NUMBER')) {
            $successPayment = false;
        }

        if ($gateResponse['errorCode'] != 0 || ($gateResponse['orderStatus'] != 1 && $gateResponse['orderStatus'] != 2)) {
            $successPayment = false;
        }

//        if ($gateResponse['orderStatus'] == 1) {
//            $depositResult = $this->confirm($payment);
//            if (!empty($depositErrors = $depositResult->getErrors())) {
//                $result->addErrors($depositErrors);
//                $successPayment = false;
//            }
//        }

        if ($request->get('operation') === 'declinedByTimeout'
            && Option::get($moduleId, 'CANCEL_ORDER_BY_TIMEOUT')
            && $request->get('CALLBACK_REDIRECT') == 1
            && !$payment->isPaid()
        ) {
            $this->cancelOrder($payment->getOrderId());
            return $result;
        }

        if ($successPayment && !$payment->isPaid()) {
            $inputJson = self::encode($request->toArray());

            $fields = array(
                'PS_INVOICE_ID' => $request->get('mdOrder'),
                "PS_STATUS_CODE" => $gateResponse['orderStatus'],
                "PS_STATUS_DESCRIPTION" => $gateResponse["cardAuthInfo"]["pan"] . ";" . $gateResponse['cardAuthInfo']["cardholderName"],
                "PS_SUM" => $gateResponse["amount"] / 100,
                "PS_STATUS" => 'Y',
                "PS_CURRENCY" => $gateResponse["currency"],
                "PS_RESPONSE_DATE" => new DateTime()
            );

            $paySystemVal = $fields['PS_INVOICE_ID'];

            $result->setOperationType(PaySystem\ServiceResult::MONEY_COMING);
            $result->setPsData($fields);

            $order = Order::load($payment->getOrderId());

            // set order status
            $option_order_status = Option::get($moduleId, 'RESULT_ORDER_STATUS');

            if ($option_order_status != 'FALSE') {
                $statuses = array();
                $dbStatus = \CSaleStatus::GetList(array("SORT" => "ASC"), array("LID" => LANGUAGE_ID), false, false,
                    array("ID", "NAME", "SORT"));
                while ($arStatus = $dbStatus->GetNext()) {
                    $statuses[$arStatus["ID"]] = "[" . $arStatus["ID"] . "] " . $arStatus["NAME"];
                }

                if (array_key_exists($option_order_status, $statuses)) {
                    $order->setField('STATUS_ID', $option_order_status);
                } else {
                    echo '<span style="display:block; font-size:16px; display:block; color:red;padding:20px 0;">ERROR! CANT CHANGE ORDER STATUS</span>';
                }
            }

            // Установка ID оплаты в платежной системе для заказа
            $obPropPaysystem = $order->getPropertyCollection()->getItemByOrderPropertyId(
                $this->arPropsIds['PAYSYSTEM_ID'][$order->getPersonTypeId()]
            );

            if ($obPropPaysystem) {
                if ($obPropPaysystem->getValue() == '') {
                    $obPropPaysystem->setValue($paySystemVal);
                }
            }

            // set delivery status
            if ($this->getBusinessValue($payment, 'SBERBANK_HANDLER_SHIPMENT') == 'Y') {
                $shipmentCollection = $order->getShipmentCollection();
                foreach ($shipmentCollection as $shipment) {
                    if (!$shipment->isSystem()) {
                        $shipment->allowDelivery();
                    }
                }
            }

            $order->save();
        }


        if ($request->get('CALLBACK_REDIRECT') == 1) {
            require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/sberbank.ecom3/config.php');
            //require dirname(dirname(__FILE__)) . '/config.php';

            if ($SBERBANK_CONFIG['CALLBACK_BROADCAST']) {
                $broadcast_url = Option::get($moduleId, 'CALLBACK_REDIRECT_BROADCAST', '');
                $queryParams = $request->getRequestMethod() == 'GET' ? $request->getQueryList()->toArray() : $request->getPostList()->toArray();
                if (strlen($broadcast_url) > 5) {
                    $gateway->broadcast_callback($broadcast_url, $queryParams);
                }
            }

            return $result;
        }

        $returnPage = $this->getBusinessValue($payment, 'SBERBANK_RETURN_URL');
        $failPage = $this->getBusinessValue($payment, 'SBERBANK_FAIL_URL');

        if (strlen($returnPage) > 4 && $successPayment) {
            echo "<script>window.location='" . $this->getOrderDescription($payment,
                    'SBERBANK_RETURN_URL') . "'</script>";
        } else {
            if (strlen($failPage) > 4 && !$successPayment) {
                echo "<script>window.location='" . $this->getOrderDescription($payment,
                        'SBERBANK_FAIL_URL') . "'</script>";
            } else {
                self::printResultText($payment, $successPayment);
            }
        }


        return $result;
    }

    public function getPaymentIdFromRequest(Request $request)
    {
        $paymentId = $request->get('PAYMENT_ID');
        return intval($paymentId);
    }

    public function getCurrencyList()
    {
        return array('RUB', 'EUR', 'USD', 'UAH', 'BYN');
    }

    public static function getIndicativeFields()
    {
        return array('PAYMENT' => 'SBERBANK');
    }

    static protected function isMyResponseExtended(Request $request, $paySystemId)
    {
        global $APPLICATION;

        $gateway = new \Sberbank\Payments\Gateway;

        if ($request->get('OPERATION_CALLBACK') == 'SBER' && $request->get('CALLBACK_REDIRECT') != 1) {
            if (!$request->get('orderNumber')) {
                $gateway->baseLogger('CALLBACK_RETURN', 'CALLBACK', $request->getQueryList(), [],
                    'ERROR PROCESSING CALLBACK');
                return false;
            }

            $arrOrderIds = explode("_", $request->get('orderNumber'));
            array_pop($arrOrderIds);
            $R_PAYMENT_ID = array_pop($arrOrderIds);
            $R_ORDER_ID = implode('_', $arrOrderIds);

            if (!$R_ORDER_ID) {
                return false;
            }

            $order = is_numeric($R_ORDER_ID) ? Order::load($R_ORDER_ID) : false;

            if ($order) {
                $paymentCollection = $order->getPaymentCollection();
                foreach ($paymentCollection as $payment) {
                    if ($R_PAYMENT_ID == $payment->getId()) {
                        LocalRedirect($APPLICATION->GetCurUri("ORDER_ID=" . $payment->getOrderId() . "&PAYMENT_ID=" . $R_PAYMENT_ID . "&CALLBACK_REDIRECT=1" . "&operation=" . $request->get('operation')));
                    }
                }
            }

            $order = Order::loadByAccountNumber($R_ORDER_ID);
            if ($order) {
                $paymentCollection = $order->getPaymentCollection();
                foreach ($paymentCollection as $payment) {
                    if ($R_PAYMENT_ID == $payment->getId()) {
                        LocalRedirect($APPLICATION->GetCurUri("ORDER_ID=" . $payment->getOrderId() . "&PAYMENT_ID=" . $R_PAYMENT_ID . "&CALLBACK_REDIRECT=1" . "&operation=" . $request->get('operation')));
                    }
                }
            }
            if (!$order) {
                $gateway->baseLogger('CALLBACK_RETURN', 'CALLBACK', $request->getQueryList(), [],
                    'ERROR PROCESSING CALLBACK');
                return false;
            }
            return false;
        }


        if (!$request->get('ORDER_ID')) {
            return false;
        }

        $order = is_numeric($request->get('ORDER_ID')) ? Order::load($request->get('ORDER_ID')) : false;

        if (!$order) {
            $order = Order::loadByAccountNumber($request->get('ORDER_ID'));
        }
        if (!$order) {
            echo Loc::getMessage('RBS_MESSAGE_ERROR_BAD_ORDER');
            return false;
        }

        $paymentIds = $order->getPaymentSystemId();
        return in_array($paySystemId, $paymentIds);
    }

    private function getPropertyValueByCode($propertyCollection, $code)
    {
        $property = '';
        foreach ($propertyCollection as $property) {
            if ($property->getField('CODE') == $code) {
                return $property->getValue();
            }
        }
    }

    /**
     * @return array
     */
    protected function getUrlList()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getProps()
    {
        $data = array();

        return $data;
    }

    /**
     * @param Payment $payment
     * @param Request $request
     * @return bool
     */
    public function initPrePayment(Payment $payment = null, Request $request)
    {
        return true;
    }

    /**
     * @param array $orderData
     */
    public function payOrder($orderData = array())
    {
    }

    /**
     * @param array $orderData
     * @return bool|string
     */
    public function BasketButtonAction($orderData = array())
    {
        return true;
    }

    /**
     * @param array $orderData
     */
    public function setOrderConfig($orderData = array())
    {
        if ($orderData) {
            $this->prePaymentSetting = array_merge($this->prePaymentSetting, $orderData);
        }
    }

    protected function getOrderDescription(Payment $payment, $PROP_CODE)
    {
        /** @var PaymentCollection $collection */
        $collection = $payment->getCollection();
        $order = $collection->getOrder();
        $userEmail = $order->getPropertyCollection()->getUserEmail();

        $description = str_replace(
            array(
                '#PAYMENT_NUMBER#',
                '#ORDER_NUMBER#',
                '#PAYMENT_ID#',
                '#ORDER_ID#',
                '#USER_EMAIL#'
            ),
            array(
                $payment->getField('ACCOUNT_NUMBER'),
                $order->getField('ACCOUNT_NUMBER'),
                $payment->getId(),
                $order->getId(),
                ($userEmail) ? $userEmail->getValue() : ''
            ),
            $this->getBusinessValue($payment, $PROP_CODE)
        );

        return $description;
    }

    private static function encode(array $data)
    {
        return \Bitrix\Main\Web\Json::encode($data, JSON_UNESCAPED_UNICODE);
    }

    protected function printResultText($payment, $successPayment)
    {
        global $APPLICATION;
        echo '<div class="sberbank-center" style="width: 100%;display: flex;align-items: center;align-content: center;justify-content: center;height: 100%;position: fixed;"><div style="display: block;background:#fff;padding: 10px 10px; margin-left:-10px;border-radius: 6px;max-width: 400px; border: 1px solid #e7e7e7;">';
        echo '<div class="sberbank-result-message" style="margin:5px; text-align:center;padding:10px 20px; 0"><span style=" font-family: arial;font-size: 16px;">';

        if ($successPayment) {
            $APPLICATION->SetTitle(Loc::getMessage('SBERBANK_PAYMENT_MESSAGE_THANKS'));
            echo Loc::getMessage('SBERBANK_PAYMENT_MESSAGE_THANKS_DESCRIPTION') . $this->getBusinessValue($payment,
                    'SBERBANK_ORDER_NUMBER');
        } else {
            $APPLICATION->SetTitle(Loc::getMessage('SBERBANK_PAYMENT_MESSAGE_ERROR'));
            echo Loc::getMessage('SBERBANK_PAYMENT_MESSAGE_ERROR') . ' #' . $this->getBusinessValue($payment,
                    'SBERBANK_ORDER_NUMBER');
        }
        echo '<div style=" display: block; margin:10px 10px 0;"><a style="font-family: arial;font-size: 16px;color: #21a038;" href="/">' . Loc::getMessage('SBERBANK_RETURN_LINK') . '</a></div>';
        echo "</span></div>";
        echo "</div></div>";
    }


    private function cancelOrder($orderId)
    {
        $order = Order::load($orderId);
        $order->setField("CANCELED", "Y");
        $order->setField("REASON_CANCELED", Loc::getMessage('SBERBANK_CANCEL_ORDER_MESSAGE'));
        $order->save();
    }



//    public static function findMyDataRefundablePage(array $paySystemList)
//    {
//        $result = array();
//        $personTypeList = BusinessValue::getPersonTypes();
//        $handler = PaySystem\Manager::getFolderFromClassName(get_called_class());
//        $description = PaySystem\Manager::getHandlerDescription($handler);
//
//        foreach ($paySystemList as $data)
//        {
//            var_dump($data);
//            foreach ($personTypeList as $personType)
//            {
////                $shopId = BusinessValue::get('YANDEX_SHOP_ID', PaySystem\Service::PAY_SYSTEM_PREFIX.$data['ID'], $personType['ID']);
////                if ($shopId && !isset($result[$shopId]))
////                {
////                    $cert = PaySystem\YandexCert::getCert($shopId);
////                    $result[$shopId] = array(
////                        'EXTERNAL_ID' => $shopId,
////                        'NAME' => $description['NAME'],
////                        'HANDLER' => 'yandex',
////                        'LINK_PARAMS' => 'shop_id='.$shopId,
////                        'CONFIGURED' => ($cert) ? 'Y' : 'N'
////                    );
////                }
//            }
//        }die();
//
//        return $result;
//    }


    public function isRefundableExtended()
    {
        return true;
    }

    public function confirm(Payment $payment)
    {
        $moduleId = 'sberbank.ecom3';
        $handler_logging = $this->getBusinessValue($payment, 'SBERBANK_HANDLER_LOGGING') == 'Y' ? 1 : 0;


        $result = new PaySystem\ServiceResult();
        $gateway = new \Sberbank\Payments\Gateway();

        $gateway->setOptions(array(
            // module settings
            'gate_url_prod' => Option::get($moduleId, 'SBERBANK_PROD_URL'),
            'gate_url_test' => Option::get($moduleId, 'SBERBANK_TEST_URL'),
            'test_mode' => $this->getBusinessValue($payment, 'SBERBANK_GATE_TEST_MODE') == 'Y' ? 1 : 0,
            'handler_logging' => $handler_logging
        ));


        $data = array(
            'userName' => $this->getBusinessValue($payment, 'SBERBANK_GATE_LOGIN'),
            'password' => $this->getBusinessValue($payment, 'SBERBANK_GATE_PASSWORD'),
            'orderId' => $payment->getField('PS_INVOICE_ID'),
            'amount' => $this->getBusinessValue($payment, 'SBERBANK_ORDER_AMOUNT')
        );
        $gateway->buildData($data);
        $gateResponse = $gateway->deposit();

        if ($gateResponse['errorCode'] != 0) {
            return $result->addError(new Error($gateResponse['errorMessage']));
        }

        $gateway->baseLogger('DEPOSIT', 'DEPOSIT', $data, $gateResponse, 'DEPOSIT INFO');

        return $result;
    }

    public function cancel(Payment $payment)
    {
        $moduleId = 'sberbank.ecom3';
        $handler_logging = $this->getBusinessValue($payment, 'SBERBANK_HANDLER_LOGGING') == 'Y' ? 1 : 0;


        $result = new PaySystem\ServiceResult();
        $gateway = new \Sberbank\Payments\Gateway();

        $gateway->setOptions(array(
            // module settings
            'gate_url_prod' => Option::get($moduleId, 'SBERBANK_PROD_URL'),
            'gate_url_test' => Option::get($moduleId, 'SBERBANK_TEST_URL'),
            'test_mode' => $this->getBusinessValue($payment, 'SBERBANK_GATE_TEST_MODE') == 'Y' ? 1 : 0,
            'handler_logging' => $handler_logging
        ));


        $data = array(
            'userName' => $this->getBusinessValue($payment, 'SBERBANK_GATE_LOGIN'),
            'password' => $this->getBusinessValue($payment, 'SBERBANK_GATE_PASSWORD'),
            'orderId' => $payment->getField('PS_INVOICE_ID'),
        );
        $gateway->buildData($data);
        $gateResponse = $gateway->reverse();

        if ($gateResponse['errorCode'] != 0) {
            return $result->addError(new Error($gateResponse['errorMessage']));
        }

        $gateway->baseLogger('CANCEL', 'CANCEL', $data, $gateResponse, 'CANCEL INFO');

        return $result;
    }

    public function refund(Payment $payment, $refundableSum)
    {
        $moduleId = 'sberbank.ecom3';
        $handler_logging = $this->getBusinessValue($payment, 'SBERBANK_HANDLER_LOGGING') == 'Y' ? 1 : 0;
        $gateway = new \Sberbank\Payments\Gateway();
        $checkGateway = new \Sberbank\Payments\Gateway();
        $result = new PaySystem\ServiceResult();
        $orderId = $payment->getField('PS_INVOICE_ID');
        $gwData = array(
            // module settings
            'gate_url_prod' => Option::get($moduleId, 'SBERBANK_PROD_URL'),
            'gate_url_test' => Option::get($moduleId, 'SBERBANK_TEST_URL'),
            'test_mode' => $this->getBusinessValue($payment, 'SBERBANK_GATE_TEST_MODE') == 'Y' ? 1 : 0,
            'handler_logging' => $handler_logging
        );


        $gateway->setOptions($gwData);
        $checkGateway->setOptions($gwData);

        $baseData = array(
            'userName' => $this->getBusinessValue($payment, 'SBERBANK_GATE_LOGIN'),
            'password' => $this->getBusinessValue($payment, 'SBERBANK_GATE_PASSWORD'),
            'orderId' => $orderId
        );

        $checkData = $baseData;
//        $checkData['orderId'] = $payment->getOrderId();
        $checkGateway->buildData($checkData);

        $checkGateResponse = $checkGateway->checkOrder();

        $data = $baseData;
        if ($checkGateResponse['orderStatus'] == 2) {
            $data = $baseData;
            $data['amount'] = $refundableSum * 100;

            $gateway->buildData($data);
            $gateResponse = $gateway->refund();

            if ($gateResponse['errorCode'] != 0) {
                $result->addError(new Error($gateResponse['errorMessage']));
            }
        }
        if ($checkGateResponse['orderStatus'] == 1) {
            $data = $baseData;
            $gateway->buildData($data);
            $gateResponse = $gateway->reverse();

            if ($gateResponse['errorCode'] != 0) {
                $result->addError(new Error($gateResponse['errorMessage']));
            }
        }

        $result->setOperationType(PaySystem\ServiceResult::MONEY_LEAVING);

        $gateway->baseLogger('REFUND', 'REFUND', $data, $gateResponse, 'REFUND INFO');

        return $result;
    }
//    public function refund(Payment $payment, $refundableSum): PaySystem\ServiceResult
//    {
//        $result = new PaySystem\ServiceResult();
//
//        $url = $this->getUrl($payment, 'refund.do');
//        $params = $this->getMerchantParams($payment);
//        $params['orderId'] = $payment->getField('PS_INVOICE_ID');
//        $params['amount'] = $refundableSum * 100;
//
//        $sendResult = $this->send($url, $params);
//        if (!$sendResult->isSuccess())
//        {
//            $result->addErrors($sendResult->getErrors());
//
//            $error = static::class.': refund: '.implode("\n", $sendResult->getErrorMessages());
//            PaySystem\Logger::addError($error);
//
//            return $result;
//        }
//
//        $result->setOperationType(PaySystem\ServiceResult::MONEY_LEAVING);
//
//        return $result;
//    }


    public function check(Payment $payment)
    {
        // TODO: Implement check() method.
    }


    public function sendResponse(PaySystem\ServiceResult $result, Request $request)
    {
    }

    private function getTotalOrderSumCoeff($orderId)
    {
        // тут метод холдирования по коэффициенту
    }

}
