<?php

$SBERBANK_CONFIG = array(
	'MODULE_ID' => 'sberbank.ecom3',
	'BANK_NAME' => 'Sberbank',
    'SBERBANK_PROD_URL' => 'https://ecommerce.sberbank.ru/ecomm/gw/partner/api/v1/',
    'SBERBANK_TEST_URL' => 'https://ecomtest.sberbank.ru/ecomm/gw/partner/api/v1/',
//    'SBERBANK_TEST_URL' => 'https://ecomdoc.sberbank.ru/ecomm/gw/partner/api/v1/',
	'ISO' => array(
	    'USD' => 840,
	    'EUR' => 978,
	    'RUB' => 643,
	    'RUR' => 643,
	    'BYN' => 933
	),
	'MODULE_VERSION' => '1.0.2',
	'CALLBACK_BROADCAST' => false,
	'DISCOUNT_HELPER' => false,
	'IGNORE_PRODUCT_TAX' => false,
	"MEASUREMENT_NAME" => 'шт', //FFD v.1.05
	"MEASUREMENT_CODE" => 0, //FFD v.1.2
	"RBS_ENABLE_CALLBACK" => true,
	'CANCEL_ORDER_BY_TIMEOUT' => false,
);

