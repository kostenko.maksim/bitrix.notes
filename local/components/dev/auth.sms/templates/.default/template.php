<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */

$this->setFrameMode(true);

?>

<div class="modal fade auth" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
    <div class="modal-dialog auth__modal-dialog" role="document">
        <div class="modal-content auth__modal-content">

            <div class="modal-body auth__modal-body"> 
                <button type="button" class="close auth__close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="auth__step active">
                    <div class="auth__headline">
                        <strong>Войдите или зарегистрируйтесь, чтобы продолжить</strong>
                    </div>
                    <div class="auth__form-wrapper">
                        <div class="auth__warning_phone"></div>
                        <form class="auth__form">
                            <div class="auth__input-holder">
                                <label class="auth__input-label">Телефон</label>
                                <input class="auth__input" type="tel" name="PHONE" placeholder="+7 (___) ___-__-__" />
                                <span class="error_phone help-block text-danger"></span>
                            </div>
                            <div>
                                <button class="btn btn-blue auth__btn has-spinner btn-block" type="submit" >Получить SMS-код</button>
                            </div>
                            <div class="checkbox-policy">
                                <label>
                                    <input type="checkbox" id="SMS_POLICY" name="SMS_POLICY" checked>
                                    <span class="label-text">Я согласен на обработку персональных данных и ознакомился с <a href="/help/konfidentsialnaya-informatsiya/" target="_blank">Политикой конфиденциальности</a></span>
                                </label>
                            </div>
                            <a href="/auth/" class="auth__by-email"><strong>Войти по почте</strong></a>
                        </form>
                    </div>
                </div>

                <div class="auth__step">
                    <div class="auth__headline">
                        <strong>Введите код подтверждения</strong>
                    </div>
                    <div class="auth__sub-headline">
                        Мы отправили код подтверждения на номер<br>
                        <span class="auth__sent-to"></span> <a href="#" class="auth__change-number"><strong>Изменить</strong></a>   
                    </div>
                    <div class="auth__warning"></div>
                    <form class="auth__form-codes">
                        <div class="auth__codes request_wait">
                            <input maxlength="1" class="auth__code" type="text" name="code_1" />
                            <input maxlength="1" class="auth__code" type="text" name="code_2" />
                            <input maxlength="1" class="auth__code" type="text" name="code_3" />
                            <input maxlength="1" class="auth__code" type="text" name="code_4" />
                        </div>
                    </form>
                    <div class="auth__allowed-in">
                        <strong>Повторная отправка доступна через <span id="auth_count_down" class="auth__allowed-in-seconds"></span></strong>
                    </div>
                    <a href="#" class="btn btn-blue auth__resend-btn d-none btn-block">Получить код повторно</a>

                    <a href="/auth/" class="auth__by-email"><strong>Войти по почте</strong></a>
                </div>

                <div class="auth__step">
                    <div class="d-flex justify-content-center align-items-center flex-column h-100">
                        <div class="success-checkmark">
                            <div class="check-icon">
                                <span class="icon-line line-tip"></span>
                                <span class="icon-line line-long"></span>
                                <div class="icon-circle"></div>
                                <div class="icon-fix"></div>
                            </div>
                        </div>
                        <div class="auth__text-success">
                            Вы авторизовались успешно!
                        </div>
                    </div>
                </div>    

            </div><!--/modal-body-->
        </div>
    </div>
</div> 







