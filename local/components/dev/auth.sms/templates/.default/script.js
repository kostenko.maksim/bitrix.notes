$(function(){
    let CodeResponseConfirmCode = {
        0: "Успешный статус работы метода",
        5: "Неправильный код. Попробуйте еще раз",
        6: "Недействительный код, запросите новый"
    };
    let CodeResponseGetCode = {
        2: "Неверный номер телефона",
    };
    ////////////////////// Шаг 1 - Получаем номер телефона
    $(document).on('submit', '.auth__form', function(e){
        e.preventDefault();
        
        var phone = $('.auth__input').val().replace(/[^0-9]+/g, "");
        var policy = $('#SMS_POLICY');
        $('.auth__input-holder .error_phone').html('');
        $('.checkbox-policy .label-text').removeClass('text-danger');
        if(phone.length != 11 ){
            $('.auth__input-holder .error_phone').html('Введен некорректный номер телефона');
        }else if(!$(policy).is(":checked")){
            $('.checkbox-policy .label-text').addClass('text-danger');
        }else{
            $(document).find('.auth__sent-to').html($('.auth__input').val());
            let container = $(this);
            let button = $(container).find('.auth__btn');
            $(button).buttonLoader('start');
            var request = BX.ajax.runComponentAction('dev:auth.sms', 'getCode', {
                mode:'class',
                data: {
                    phone: phone
                }
            });
            request.then(function(response){
                $(button).buttonLoader('stop');
                code = response.data.result.Code;
                if(code == '0' || code == '7'){
                    MoveNextStep(container);
                    $('.auth__warning_phone').html('');
                    timerInit();
                }else{
                    let msg = "Ошибка, попробуйте позже";
                    if(CodeResponseGetCode[code]){
                        msg = CodeResponseGetCode[code];
                    }
                    $('.auth__warning_phone').html('<div class="auth__warning_phone alert alert-warning" role="alert">'+msg+'</div>');
                }
            });
        }
    });
    $('.auth__input').keydown(function(e) {
        let phone = $('.auth__input-holder .auth__input').val().replace(/[^0-9]+/g, "");
        if(!phone.length){
            $('.auth__input-holder .auth__input').val('7');
        }
    });
    $('.auth__input').keyup(function(e) {
        let phone = $('.auth__input-holder .auth__input').val().replace(/[^0-9]+/g, "");
        if(phone.length == 1 && phone != 7){
            $('.auth__input-holder .auth__input').val('7');
        }
    });

    ////////////////////// Шаг 2 - Получаем ползователем введены код из смс
    var EnteredCode = {};
    var inputs = $('.auth__codes .auth__code');
    $(document).on('keyup', '.auth__codes.request_wait .auth__code', function(key){
        if(key.keyCode == 8){
            if($(this).prev()){
                $(this).prev().val('');
                $(this).prev().focus();
            }
        }else{
            this.value = this.value.replace(/[^0-9]/g, ''); // Позволяет вводить только цыфры
            var auth__code = $(this).val();

            if(auth__code){
                if($(this).next()){
                    $(this).next().focus();
                }
            }
            code = "";
            // Собираем введены код
            inputs.each(function() {
                EnteredCode[this.name] = $(this).val();
                code = code + $(this).val();
            });

            if(EnteredCode.code_1 && EnteredCode.code_2 && EnteredCode.code_3 && EnteredCode.code_4){
                polarShowPreloader();
                let phone = $('.auth__input').val().replace(/[^0-9]+/g, "");
                formCodeConfirme('block');
                var request = BX.ajax.runComponentAction('dev:auth.sms', 'confirmCode', {
                    mode:'class',
                    data: {
                        phone: phone,
                        code: code
                    }
                });
                request.then(function(response){
                    code = response.data.result.Code;
                    if(code == '0'){
                        $('.auth__warning').html('');   

                        // Carrotquest 
                        if(typeof carrotquest == "object" && carrotquest !== undefined){
                            carrotquest.auth(response.data.result.carrotquest.userId, response.data.result.carrotquest.hash);
                            carrotquest.track("$registered", {"$email": response.data.result.carrotquest.userEmail, "$name": response.data.result.carrotquest.userName});
                        }
                        
                        window.location.href = window.location.href;
                    } else {
                        polarHidePreloader();
                        formCodeConfirme('unblock');
                        let msg = "Ошибка, попробуйте позже";
                        if(CodeResponseConfirmCode[code]){
                            msg = CodeResponseConfirmCode[code];
                        }
                        $('.auth__warning').html('<div class="auth__warning alert alert-warning" role="alert">'+msg+'</div>');
                        $('.auth__code').val('');
                    }  
                    
                });         
            }
        }
    });

    var clearIntervalCounter;

    $(document).find('.auth__change-number').click(function(){
        MoveFirstStep($(this));
        clearIntervalCounter = true;
        $('#auth_count_down').text('05:00');
        $('.auth__warning').html('');
    });

    $(document).find('.auth__resend-btn').click(function(e){
        let button = $(this);
        let phone = $('.auth__input').val().replace(/[^0-9]+/g, "");
        $(button).buttonLoader('start');
        var request = BX.ajax.runComponentAction('dev:auth.sms', 'getCode', {
            mode:'class',
            data: {
                phone: phone
            }
        });
        request.then(function(response){
            $(button).buttonLoader('stop');
            code = response.data.result.Code;
            if(code == '0' || code == '7'){
                $(button).addClass('d-none');
                $('.auth__allowed-in').removeClass('d-none');
                formCodeConfirme('unblock');
                timerInit();
            }else{
                let msg = "Ошибка, попробуйте позже";
                if(CodeResponseGetCode[code]){
                    msg = CodeResponseGetCode[code];
                }
                $('.auth__warning_phone').html('<div class="auth__warning_phone alert alert-warning" role="alert">'+msg+'</div>');
            }
        });
        e.preventDefault();
    });

    function startTimer(duration, display) {
        var finalText;
        var timer = duration, minutes, seconds;
        var intervalCounter = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            finalText = minutes + ":" + seconds;

            if(finalText == '00:00') {
                $('.auth__allowed-in').addClass('d-none');
                $('.auth__resend-btn').removeClass('d-none');
                clearInterval(intervalCounter);
                formCodeConfirme('block');
                $('.auth__code').val('');
                $('.auth__warning').html('');
            }
            if(clearIntervalCounter) {
                clearInterval(intervalCounter);
                clearIntervalCounter = false;
            }

            display.text(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    function timerInit(){
        var fiveMinutes = 60 * 2;
        var minutesInTxt = '02:00';
        var display = $('#auth_count_down');
        display.text(minutesInTxt);
        startTimer(fiveMinutes, display); 
    }

    function MoveFirstStep(thisEl){
        thisEl.closest('.auth__step').removeClass('active');
        thisEl.closest('.auth__step').prev().addClass('active');
    }       

    function MoveNextStep(thisEl){
        thisEl.closest('.auth__step').removeClass('active');
        thisEl.closest('.auth__step').next().addClass('active');
    }    

    function AnimateCheckedIcon(){
        $(".check-icon").hide();
        setTimeout(function () {
            $(".check-icon").show();
        }, 10);
    }

    function formCodeConfirme(action)
    {
        if(action == 'block')
        {
            $('.auth__codes').removeClass('request_wait');
            $( ".auth__code" ).prop( "disabled", true );
        }
        if(action == 'unblock')
        {
            $('.auth__codes').addClass('request_wait');
            $( ".auth__code" ).prop( "disabled", false );
        }
    }

});

