<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;
use AB\Iblock\Element;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;

Loader::includeModule('iblock');

class AuthSms extends \CBitrixComponent implements Controllerable
{

    private $userId;

    public function configureActions()
    {
        return [
            'getCode' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
            'confirmCode' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ]
        ];
    }

    public function prepareParams()
    {
        $this->arParams['PREFIX_PHONE'] = '900 901 902 903 904 905 906 908 909 910 911 912 913 914 915 916 917 918 919 920 921 922 923 924 925 926 927 928 929 930 931 932 933 934 936 937 938 939 941 950 951 952 953 954 955 956 958 960 961 962 963 964 965 966 967 968 969 970 971 977 978 980 981 982 983 984 985 986 987 988 989 991 992 993 994 995 996 997 999';
        $this->arParams['SPAM_PHONE'] = [
            '71111111111',
            '72222222222',
            '73333333333',
            '74444444444',
            '75555555555',
            '76666666666',
            '77777777777',
            '78888888888',
            '79999999999',
            '70000000000',
            '79111111111',
            '79222222222',
            '79333333333',
            '79444444444',
            '79555555555',
            '79666666666',
            '79777777777',
            '79888888888',
            '79000000000',
            '78111111111',
            '78222222222',
            '78333333333',
            '78444444444',
            '78555555555',
            '78666666666',
            '78777777777',
            '78999999999',
            '78000000000',
            '11111111111',
            '22222222222',
            '33333333333',
            '44444444444',
            '55555555555',
            '66666666666',
            '77777777777',
            '88888888888',
            '99999999999',
            '00000000000',
            '80000000000'
        ];

        $this->arParams['API_KEY'] = 'XXX-KEY-XXX'; // достаем ключик из БД

        return $this->arParams;
    }

    public function prepareResult()
    {
        $this->arResult["USER"] = "";
        return $this->arResult;
    }

    public function executeComponent()
    {
        $arParams = $this->prepareParams();
        $arResult = $this->prepareResult();

        if ($arParams["CACHE_TYPE"] == "Y") {
            if ($this->StartResultCache($arParams["CACHE_TIME"])) {
                $this->includeComponentTemplate();
            }
        } else {
            $this->includeComponentTemplate();
        }
    }

    public function getCodeAction($phone)
    {
        $result = $this->requestDevinotele('GenerateCode',['phone' => $phone]);

        return [
            'result' => $result
        ];
    }

    public function confirmCodeAction($phone, $code, $isSubscribed)
    {
        $result = $this->requestDevinotele('CheckCode',['phone' => $phone, 'code' => $code]);
        if($result['Code'] == '0'){
            $result['carrotquest'] = $this->loginByPhone($phone);
            $result['statusUpdateSubscribed'] = $this->updateSubscribed($isSubscribed);
        }
        return [
            'result' => $result
        ];
    }

    private function requestDevinotele($action, $data)
    {
        $url = 'https://phoneverification.devinotele.com/'.$action;
        $data = array('DestinationNumber' => $data['phone'], 'Code' => $data['code']);

        // защита для отправки SMS
        if ($action === 'GenerateCode') {
            $checkedRequest = $this->isCheckRequest($data['DestinationNumber']);
            if (!$checkedRequest['success'])
                return ['Code' => $checkedRequest['Code']];
        }

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/local/logs/success_sms_'.date('Y-m-d').'.txt', $data['DestinationNumber'].' => '.$action. PHP_EOL, FILE_APPEND);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            'X-ApiKey: '.$this->arParams['API_KEY'],
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($curl);
        $result = json_decode($result, true);

        curl_close($curl);
        return $result;
    }

    private function loginByPhone($phone){
        global $USER;
        if (!$USER->IsAuthorized() && $phone){
            $arDataForFindUser['PERSONAL_PHONE'] = $phone;
            $userId = TLogic::findOrCreateUser($arDataForFindUser); //тут метод ищем пользователя или создаем нового
            if($userId) {
                $this->userId = $userId;
                $USER->Authorize($userId);
                if($USER->IsAuthorized()){
                    $arUserData = [
                        'userId' => $USER->GetID(),
                        'userName' => $USER->GetFirstName(),
                        'userPhone' => $phone,
                        'userEmail' => $USER->GetEmail()
                    ];
                    return $this->carrotquestGen($arUserData);
                }
            }
        }
    }


    /**
     * Метод обновляет свойство пользователя UF_SUBSCRIBED
     * согласие на получение информационной и рекламной рассылки
     *
     * @param $isSubscribed
     * @return string
     */
    private function updateSubscribed($isSubscribed): string
    {
        if (empty($this->userId))
            return 'Ошибка получения пользователя';

        $obUser = new \CUser;
        $arFields = Array(
            "UF_SUBSCRIBED" => $isSubscribed === 'true',
        );
        $status = 'UF_SUBSCRIBED обновлено';
        if (!$obUser->Update($this->userId, $arFields)) {
            $status = 'Error: '.$obUser->LAST_ERROR;
        }

        return $status;
    }

    private function carrotquestGen($arUserData){
        $UserAuthKey = 'XXXXXXXXXXXXXXXXXXXXX-SALT-XXXXXXXXXXXXXXXXXXXXXXX';
        $arUserData['hash'] = hash_hmac('sha256', $arUserData['userId'], $UserAuthKey);
        return $arUserData;
    }

    /**
     * Метод осуществляет проверку частой отправки SMS
     * Невалидно:
     * - если номер не начинается с маски сотовых операторов
     * - не прошло 5 сек с момента захода на сайт
     * - больше 3 отправок в минуту
     * - больше 20 отправок в день
     *
     * @return array|bool[]
     */
    private function isCheckRequest($phone): array
    {
        //проверка что отправка происходит именно c формы
        if (!isset($_POST['dataForm']))
            return ['success' => false,'Code' => 3]; //Превышен лимит запросов в минуту

        // для отладки
        setcookie('DIF_TIME_START',time() - $_COOKIE['START_TIME'], 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
        setcookie('DIF_TIME_SMS',time() - $_COOKIE['START_TIME_SMS'], 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));

        $arParams = $this->prepareParams();
        $arPrefixPhone = explode(' ',$arParams['PREFIX_PHONE']);

        // две проверки на спам, можно оставить что-то одно
        if (!in_array(substr($phone,1,3),$arPrefixPhone))
            return ['success' => false,'Code' => 400]; //Спам: неверный номер телефона

        if (in_array($phone,$arParams['SPAM_PHONE']))
            return ['success' => false,'Code' => 401]; //Номер добавлен в спам

        if(isset($_SESSION['START_TIME']) && (time() - $_SESSION['START_TIME'] <= 5 ))
            return ['success' => false,'Code' => 404]; //Ошибка отправки, попробуйте позже

        if (!isset($_COOKIE['COUNT_REQUEST_SMS']))
            setcookie('COUNT_REQUEST_SMS', 0, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));

        if (!isset($_COOKIE['START_TIME_SMS']))
            setcookie('START_TIME_SMS',time(), 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));

        if (!isset($_COOKIE['COUNT_REQUEST_SMS_DAY']))
            setcookie('COUNT_REQUEST_SMS_DAY', 0, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));

        if (!isset($_COOKIE['START_TIME_SMS_DAY']))
            setcookie('START_TIME_SMS_DAY',date('Ymd'), 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));

        // проверка лимита за день
        if(date('Ymd') == $_COOKIE['START_TIME_SMS_DAY'] && $_COOKIE['COUNT_REQUEST_SMS_DAY'] >= 20) {
            return ['success' => false,'Code' => 402]; //Превышен лимит запросов в день
        } elseif(date('Ymd') != $_COOKIE['START_TIME_SMS_DAY']) {
            setcookie('START_TIME_SMS_DAY',date('Ymd'), 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
            setcookie('COUNT_REQUEST_SMS_DAY',1, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
        } else {
            setcookie('COUNT_REQUEST_SMS_DAY',$_COOKIE['COUNT_REQUEST_SMS_DAY']+1, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
        }

        // проверка лимита в минуту
        if(time() - $_COOKIE['START_TIME_SMS'] <= 60 && $_COOKIE['COUNT_REQUEST_SMS'] >= 3) {
            return ['success' => false,'Code' => 3]; //Превышен лимит запросов в минуту
        } elseif(time() - $_COOKIE['START_TIME_SMS'] > 60 && $_COOKIE['COUNT_REQUEST_SMS'] > 0) {
            setcookie('START_TIME_SMS',time(), 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
            setcookie('COUNT_REQUEST_SMS',1, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
        } else {
            setcookie('COUNT_REQUEST_SMS',$_COOKIE['COUNT_REQUEST_SMS']+1, 0, '/', str_replace(':443', '', $_SERVER['HTTP_HOST']));
        }

        return ['success' => true];
    }
}
