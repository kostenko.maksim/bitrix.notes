<?php

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");



$APPLICATION->IncludeComponent(
    "dev:auth.sms",
    "",
    array(
        "COMPONENT_TEMPLATE" => "",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
    ),
    false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");