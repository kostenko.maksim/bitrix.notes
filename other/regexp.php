<?php

/**
 * Тупенькая валидация e-mail:
 * Ограничиваем часто встречающимися корневыми доменами  * (?:ru|com|net|by|org|biz|gov|pro)
 * + стандартная валидация:
 * список разрешенных символов знаки "@" и "." и "-" и "_" после последней точки только разрешенные корневые домены
 */

$arEmail = [
    '123@mail.ru',
    'mail@mail.com',
    'mail@mail.yahoo.com',
    'mks-kms@shop-shop2.ru',
    '123_mail-gmail.dot@gmail.com',
    'test@test.test',
    'test@mail.org',
    'test@mail.by',
    'shop$@mail.ru',
    'shoping@mail.shop',
    'sdsad',
    'это_моя_почта@mail.ru',
    'may.yahooo@gmail.com',
    'Kmay.yAAhooom@gmail.COM',
    'KMAY.YAAHOOOM@GMAIL.COM',
];

$pattern = '/^(([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})@([-0-9A-Za-z\.]{2,}\.)(?:ru|com|net|by|org|biz|gov|pro))$/iu';


foreach ($arEmail as $email) {
    if (preg_match($pattern, $email ) === 1) {
        echo '&#9989;<span style="color: green;">'.$email.' соответствует шаблону</span><br>';
    } else {
        echo '&#10060;<span style="color: red;">'.$email.' невалиден</span><br>';
    }
}