<?php
/**
 * 14.10.2022 добавление скидок с купонами через Правила работы с корзиной
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;

if(!isset($_POST['PRODUCT_ID'])) die('DENIED');
if(!isset($_POST['PRICE'])) die('DENIED');
if(!CModule::IncludeModule("catalog")) die('CATALOG_NOT_INCLUDED');
if(!CModule::IncludeModule("iblock")) die('IBLOCK_NOT_INCLUDED');
if(!CModule::IncludeModule("sale")) die('SALE_NOT_INCLUDED');

$productId = intval($_POST['PRODUCT_ID']);
$productPrice = htmlspecialcharsbx($_POST['PRICE']);
// метка, если пришло 1, значит пользователь сделал больше трех кликов на получение скидки
$checkRandom = intval($_POST['CHECK_RANDOM']);
// метка, если пришло 1, значит предоставляем пользователю максимальную скидку
$checkMaxDiscount = intval($_POST['CHECK_MAX_DISCOUNT']);

$arProduct = array();
$result = array();

// какая то логика по получению величины скидки для товара

//отсюда создаем массив возможных значений скидок
$minPrice = $productPrice * ((100 - $arProduct['MAX_DISCOUNT_VALUE']) / 100);

$arDiscount = array();

for($i = 1; $i <= 5; $i++){
    $discPrice = round($productPrice - ($productPrice - $minPrice) * ($i * 0.2), -1);
    $arDiscount[] = $discPrice;
}

$arDiscount = array_unique($arDiscount);
$arDiscount = array_values($arDiscount);

if($arDiscount['0'] > $productPrice)
    $arDiscount['0'] = $arDiscount['0'] - 10;

if($arDiscount[count($arDiscount) - 1] < $minPrice)
    $arDiscount[count($arDiscount) - 1] = $arDiscount[count($arDiscount) - 1] + 10;

$arDiscount = array_unique($arDiscount);
$arDiscount = array_values($arDiscount);

if(!isset($_SESSION['discountIndex'])) {
    $_SESSION['discountIndex'] = '0';
} else {
    if ($checkRandom) {
        if ($checkMaxDiscount) {
            $_SESSION['discountIndex'] = count($arDiscount) - 1;
        } else {
            $_SESSION['discountIndex'] = rand(0, (count($arDiscount) - 2));
        }
    } else {
        $_SESSION['discountIndex'] = rand(0, (count($arDiscount) - 2));
    }
}

// условие в корзине/заказе есть товар
$arConditions = [
    'CLASS_ID' => 'CondGroup',
    'DATA' => [
        'All' => 'AND',
        'True' => 'True',
    ],
    'CHILDREN' => [
        [
            "CLASS_ID" => "CondBsktProductGroup",
            "DATA" => [
                "Found" => "Found",
                "All" => "OR",
            ],
            "CHILDREN" => [
                [
                    "CLASS_ID" => "CondIBElement",
                    "DATA" => [
                        "logic" => "Equal",
                        "value" => $productId,
                    ]
                ]
            ]
        ],
    ],
];

// основное действие скидки: применяем фиксированную величину
$arAction = [
    "CLASS_ID" => "CondGroup",
    "DATA" => [
        "All" => "AND"
    ],
    "CHILDREN" => [
        [
            "CLASS_ID" => "ActSaleBsktGrp",
            "DATA" => [
                "Type" => "Closeout", // фиксированная цена
                "Value" => $arDiscount[$_SESSION['discountIndex']], // сама цена
                "Unit" => 'CurEach', // для каждого товара
                "Max" => 1,
                "All" => "OR",
                "True" => "True",
            ],
            "CHILDREN" => [
                [
                    "CLASS_ID" => "ActSaleSubGrp",
                    "DATA" => [
                        "All" => "AND",
                        "True" => "True",
                    ],
                    "CHILDREN" => [
                        [
                            "CLASS_ID" => "CondIBElement",
                            "DATA" => [
                                "logic" => "Equal",
                                "value" => $productId,
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];

// основные параметры для правила
$arFields = array(
    'LID'                 => SITE_ID,
    'NAME'                => 'Торг онлайн. ID товара '.$productId,
    'ACTIVE'              => 'Y',
    'ACTIVE_FROM'         => '',
    'ACTIVE_TO'           => '',
    'SORT'                => 100,
    'PRIORITY'            => 100,
    'USER_GROUPS'         => [2], //Группа пользователей для всех
    'LAST_DISCOUNT'       => 'N',
    'LAST_LEVEL_DISCOUNT' => 'N',
    'CONDITIONS'          => $arConditions,
    'ACTIONS'             => $arAction,
);

// если раннее пользователь создавал скидку на товар
if(array_key_exists($productId, $_SESSION['TORG_ONLINE'])) {
    $DISCOUNT_ID = $_SESSION['TORG_ONLINE'][$productId]['DISCOUNT_ID'];
    // обновим правило корзины, меняем цену
    $result['saleDiscountID'] = CSaleDiscount::Update($DISCOUNT_ID,$arFields);
    // получим купон нашего правила корзины
    $result['arCoupons'] =  \Bitrix\Sale\Internals\DiscountCouponTable::getList(array(
        'select' => ['COUPON'],
        'filter' => array('DISCOUNT_ID' => $DISCOUNT_ID)
    ))->fetch();
    // применим купон к корзине
    if (!empty($result['arCoupons'] && isset($result['arCoupons']['COUPON'])))
        $result['DiscountCouponsManager'] = \Bitrix\Sale\DiscountCouponsManager::add($result['arCoupons']['COUPON']);

    $_SESSION['TORG_ONLINE'][$productId] = array(
        'DISCOUNT_ID' => $result['saleDiscountID'],
        'DISCOUNT_PRICE' => $arDiscount[$_SESSION['discountIndex']]
    );
} else {
    // создаем правило корзины
    $result['saleDiscountID'] = CSaleDiscount::Add($arFields);
    if (!$result['saleDiscountID']) {
        $result['errorSaleDiscountID'] = $APPLICATION->GetException();
    } else {
        // создадим купон для нашего правила корзины
        $couponCode = 'TORG_' . mb_strtoupper(randString(16));
        $arFieldsCoupon = [
            'DISCOUNT_ID' => $result['saleDiscountID'],
            'TYPE' => \Bitrix\Sale\Internals\DiscountCouponTable::TYPE_ONE_ORDER, // использовать на один заказ,
            'COUPON' => $couponCode,
            'ACTIVE' => 'Y',
            'ACTIVE_FROM' => null,
            'ACTIVE_TO' => null,
            'MAX_USE' => 0,
        ];
        $resultCoupons = \Bitrix\Sale\Internals\DiscountCouponTable::add($arFieldsCoupon);
        if (!$resultCoupons->isSuccess()) {
            $result['errorsCoupons'] = $resultCoupons->getErrorMessages();
        }
        $result['resultCouponsGenerate'] = $couponCode;
        $result['resultCoupons'] = $resultCoupons;

        // применим купон к корзине
        $result['DiscountCouponsManager'] = \Bitrix\Sale\DiscountCouponsManager::add($couponCode);

        $_SESSION['TORG_ONLINE'][$productId] = array(
            'DISCOUNT_ID' => $result['saleDiscountID'],
            'DISCOUNT_PRICE' => $arDiscount[$_SESSION['discountIndex']]
        );
    }
}

$result['discountIndex'] = $_SESSION['discountIndex'];
$result['TORG_ONLINE'] = $_SESSION['TORG_ONLINE'];
$result['PRICE'] = $arDiscount[$_SESSION['discountIndex']];
$result['MIN_PRICE'] = end($arDiscount);

die(json_encode($result));
