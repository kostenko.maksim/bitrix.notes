<?php
/**
 * Если добавляем в корзину, не забываем делать пересчет скидки, чтобы цена была корректна на мини-корзине
 **/

$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());

// добавляем товар в корзину
$item = $basket->getExistsItem('catalog', $productId);
$resultSetFields = $item->setFields(array(
    'QUANTITY' => $productQuantity,
    'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
    'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
    'PRODUCT_PROVIDER_CLASS' => '\\Bitrix\\Catalog\\Product\\CatalogProvider',
));

// пересчет скидок
$fuser = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true));
$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $fuser);
$discountResult = $discounts->calculate();
$data = $discountResult->getData();
if (!empty($data['BASKET_ITEMS'])){
    $r = $basket->applyDiscount($data['BASKET_ITEMS']);
}