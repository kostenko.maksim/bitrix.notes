<?php

# автозагрузка классов
\Bitrix\Main\Loader::registerAutoLoadClasses(
    null,
    [
        'Custom\Restrictions' => '/local/php_interface/classes/restrictions/bitrix_custom_restrictions.php',
    ]
);

//для ограничений доставки и оплаты по купонам
AddEventHandler(
    'sale',
    'onSalePaySystemRestrictionsClassNamesBuildList',
    array(
        'Custom\Restrictions',
        'onCheckRulePayment'
    )
);
AddEventHandler(
    'sale',
    'onSaleDeliveryRestrictionsClassNamesBuildList',
    array(
        'Custom\Restrictions',
        'onCheckRulePayment'
    )
);
//END для граничений досткивки и оплаты по купонам

//ограничения по категориям
AddEventHandler(
    'sale',
    'onSalePaySystemRestrictionsClassNamesBuildList',
    array(
        'Custom\Restrictions',
        'onCheckRulePaymentSections'
    )
);