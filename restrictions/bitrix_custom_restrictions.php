<?php

namespace Custom;

class Restrictions
{
    /**
     * Ограничения для доставок и платежей, двух типов == и !=
     * @return \Bitrix\Main\EventResult
     */
    function onCheckRulePayment()
    {
        $filePayments = '/local/php_interface/classes/restrictions/payments.php';

        return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS,
            array(
                '\checkPayDiscount'    => $filePayments, // ==
                '\checkPayDiscountNot' => $filePayments, // !=
                '\onCheckRulePaymentSections' => $filePayments, // !=
            )
        );
    }

}