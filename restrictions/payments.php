<?php

use Bitrix\Sale\Services\Base;
use Bitrix\Sale\Internals\CollectableEntity;
use Bitrix\Sale\Internals\Entity;
use Bitrix\Sale\Order;

\Bitrix\Main\Loader::includeModule('highloadblock');


/**
 * Class checkPayDiscount ==
 * Класс для проверки на присутствие примененных скидок в заказе
 */
class checkPayDiscount extends Base\Restriction
{
    public static function getClassTitle()
    {
        return 'по примененным скидкам';
    }

    public static function getClassDescription()
    {
        return 'если в заказе применена одна из скидок - оплата выводится';
    }

    /**
     * Произведем проверку
     * @param $discountData
     * @param array $restrictionParams
     * @param int $serviceId
     * @return bool
     */
    public static function check($discountData, array $restrictionParams, $serviceId = 0)
    {
        // примененные скидки в корзине и заказе
        $applyDiscountList = array_merge($discountData['APPLY_BLOCKS'][0]['BASKET'], $discountData['APPLY_BLOCKS'][0]['ORDER']);

        // соберем инфо по этим скидкам
        $arApplyDiscountList = [];
        foreach ($applyDiscountList as $discountItem)
            $arApplyDiscountList[] = $discountData['DISCOUNT_LIST'][$discountItem['DISCOUNT_ID']];

        // проверим наше правило
        if(is_array($arApplyDiscountList))
        {
            foreach ($arApplyDiscountList as $arDiscount)
                if(in_array($arDiscount['REAL_DISCOUNT_ID'], $restrictionParams['DISCOUNT_LIST'])
                    && $arDiscount['APPLY'] == 'Y')
                    return true;
        }

        return false;
    }

    /**
     * Получим примененные скидки по текущему заказу
     * @param Entity $entity
     * @return array|bool|mixed
     */
    protected static function extractParams(Entity $entity)
    {
        if ($entity instanceof CollectableEntity) {
            $collection = $entity->getCollection();
            $order = $collection->getOrder();
        } elseif ($entity instanceof Order) {
            $order = $entity;
        }
        else return false;

        return $order->getDiscount()->getApplyResult();
    }

    /**
     * Получаем перечень всех скидок в магазине
     * @param int $entityId
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getParamsStructure($entityId = 0)
    {
        $arList = [];

        $resDiscount = Bitrix\Sale\Internals\DiscountTable::getList(['select' => ['ID', 'NAME', 'ACTIVE']]);
        while ($arDiscount = $resDiscount->fetch())
            $arList[$arDiscount['ID']] = $arDiscount["NAME"].' ['.$arDiscount["ID"].' / '.$arDiscount["ACTIVE"].']';

        return array(
            "DISCOUNT_LIST" => array(
                "TYPE" => "ENUM",
                'MULTIPLE' => 'Y',
                "OPTIONS" => $arList,
                "LABEL" => 'Запрет по скидкам',
            )
        );
    }
}

/**
 * Class checkPayDiscount !=
 * Класс для проверки на присутствие примененных скидок в заказе
 */
class checkPayDiscountNot extends Base\Restriction
{
    public static function getClassTitle()
    {
        return 'по не примененным скидкам';
    }

    public static function getClassDescription()
    {
        return 'если в заказе применена одна из скидок - оплата не выводится';
    }

    /**
     * Произведем проверку
     * @param $discountData
     * @param array $restrictionParams
     * @param int $serviceId
     * @return bool
     */
    public static function check($discountData, array $restrictionParams, $serviceId = 0)
    {
        // примененные скидки в корзине и заказе
        $applyDiscountList = array_merge($discountData['APPLY_BLOCKS'][0]['BASKET'], $discountData['APPLY_BLOCKS'][0]['ORDER']);

        // соберем инфо по этим скидкам
        $arApplyDiscountList = [];
        foreach ($applyDiscountList as $discountItem)
            $arApplyDiscountList[] = $discountData['DISCOUNT_LIST'][$discountItem['DISCOUNT_ID']];

        // проверим наше правило
        if(is_array($arApplyDiscountList))
        {
            foreach ($arApplyDiscountList as $arDiscount)
                if(in_array($arDiscount['REAL_DISCOUNT_ID'], $restrictionParams['DISCOUNT_LIST'])
                    && $arDiscount['APPLY'] == 'Y')
                    return false;
        }

        return true;
    }

    /**
     * Получим применненные скидки по текущему заказу
     * @param Entity $entity
     * @return array|bool|mixed
     */
    protected static function extractParams(Entity $entity)
    {
        if ($entity instanceof CollectableEntity) {
            $collection = $entity->getCollection();
            $order = $collection->getOrder();
        } elseif ($entity instanceof Order) {
            $order = $entity;
        }
        else return false;

        return $order->getDiscount()->getApplyResult();
    }

    /**
     * Получаем перечень всех скидок в магазине
     * @param int $entityId
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getParamsStructure($entityId = 0)
    {
        $arList = [];

        $resDiscount = Bitrix\Sale\Internals\DiscountTable::getList(['select' => ['ID', 'NAME', 'ACTIVE']]);
        while ($arDiscount = $resDiscount->fetch())
            $arList[$arDiscount['ID']] = $arDiscount["NAME"].' ['.$arDiscount["ID"].' / '.$arDiscount["ACTIVE"].']';

        return array(
            "DISCOUNT_LIST" => array(
                "TYPE" => "ENUM",
                'MULTIPLE' => 'Y',
                "OPTIONS" => $arList,
                "LABEL" => 'Запрет по скидкам',
            )
        );
    }
}


/**
 * Class checkPayDiscount !=
 * Класс для проверки на присутствие товара в категориях из списка
 */
class onCheckRulePaymentSections extends Base\Restriction
{
    public static function getClassTitle()
    {
        return 'по категориям';
    }

    public static function getClassDescription()
    {
        return 'если в заказе есть категория то не выводится';
    }

    /**
     * Произведем проверку
     * @param $discountData
     * @param array $restrictionParams
     * @param int $serviceId
     * @return bool
     */
    public static function check($basket, array $restrictionParams, $serviceId = 0)
    {
        $product_ids = [];
        foreach ($basket as $basketItem) {
            $product_ids[] = $basketItem->getProductId();
        }
        $sections = [];
        $groups = CIBlockElement::GetElementGroups($product_ids, true);
        while($ar_group = $groups->Fetch()){
            $sections[] = $ar_group["ID"];
        }

        if(array_intersect($sections, $restrictionParams['SECTIONS'])){
            return false;
        }

        return true;
    }


    protected static function extractParams(Entity $entity){
        if ($entity instanceof CollectableEntity) {
            $collection = $entity->getCollection();
            $order = $collection->getOrder();
        } elseif ($entity instanceof Order) {
            $order = $entity;
        }
        else return false;

        return $order->getBasket();
    }

    public static function getParamsStructure($entityId = 0){
        $arList = [];
        $rsSect = CIBlockSection::GetList(array("ID" => "ASC"), array('IBLOCK_ID' => 91, 'GLOBAL_ACTIVE'=>'Y'));
        while ($arSect = $rsSect->GetNext()){
            $arList[$arSect['ID']] = $arSect['NAME'];
        }

        return array(
            "SECTIONS" => array(
                "TYPE" => "ENUM",
                'MULTIPLE' => 'Y',
                "OPTIONS" => $arList,
                "LABEL" => 'Запрет по категориям',
            )
        );
    }
}