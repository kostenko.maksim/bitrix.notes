<?php

require 'debug.php';
require 'events.php';

use \Bitrix\Main\Loader;
Loader::includeModule('rshb.sms');

function addTaskEmail($arMessageFields) {

    global $APPLICATION;

    if (Loader::includeModule("tasks"))
    {
        //echo 'START'.'<br>';
        $from = CMailUtil::ExtractMailAddress($arMessageFields['FIELD_FROM']);
        print_r($arMessageFields);
        $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>1));
        if ($oUser = $rsUser->Fetch())
            $idCreated = $oUser["ID"];
        //получаем все почтовые адреса из полей "кому", "копия"
        /*$arr_to = CMailUtil::ExtractAllMailAddresses($arMessageFields["FIELD_TO"].",".$arMessageFields["FIELD_CC"].",".$arMessageFields["FIELD_BCC"]);
        foreach ($arr_to as $to)
        {
            if (empty($to))
                continue;
            $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("email"=>$to));
            if ($oUser = $rsUser->Fetch())
                $arr_responsible[] = $oUser["ID"];
        }*/
        $idCreated = 1;
        $userForTask[] = $idCreated;
        $dateTime = new \Bitrix\Main\Type\DateTime();
        $deadline = $dateTime->add('1 days');
        $arFields = Array(
            "TITLE" => $arMessageFields['SUBJECT'],
            "DESCRIPTION" => $arMessageFields['BODY'],
            "RESPONSIBLE_ID" => $idCreated,
            "STATUS" => 2,
            "CREATED_BY" => $idCreated,
            'DEADLINE' => $deadline,
            //'DEADLINE' => date("d.m.Y H:i:s", strtotime(("+".($time+1)." days"))),
        );

        $obTask = new CTasks;
        $ID = $obTask->add($arFields);
        $success = ($ID>0);
        if($success)
        {
            echo "Ok! => ID:$ID";
        }
        else
        {
            if($e = $APPLICATION->GetException())
                echo "Error: ".$e->GetString();
        }

        // прикрепление файла к задаче
        CModule::IncludeModule('tasks');
        $storage = Bitrix\Disk\Driver::getInstance()->getStorageByUserId($USER_ID);
        $folder = $storage->getFolderForUploadedFiles();
        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/wlog.txt");
        $file = $folder->uploadFile($arFile, array(
            'NAME' => $arFile["name"],
            'CREATED_BY' => $USER_ID
        ), array(), true);
        $FILE_ID = $file->getId();
        $oTaskItem = new CTaskItem($taskId, $userId);
        $rs = $oTaskItem->Update(array("UF_TASK_WEBDAV_FILES" => Array("n$FILE_ID")));

        if(Loader::IncludeModule("im")) {
            foreach ($userForTask as $taskUserID) {
                $TaskLink = $_SERVER['HTTP_HOST'].'/company/personal/user/' . $taskUserID . '/tasks/task/view/' . $ID . '/';
                $arMessageFields = array(
                    "TO_USER_ID"     => $taskUserID, // получатель
                    "FROM_USER_ID"   => 0, // отправитель (может быть >0)
                    "NOTIFY_TYPE"    => IM_NOTIFY_SYSTEM, // тип уведомления
                    "NOTIFY_MODULE"  => "tasks", // модуль запросивший отправку уведомления
                    "NOTIFY_TAG"     => "FAX-TASKS", // символьный тэг для группировки (будет выведено только одно сообщение), если это не требуется - не задаем параметр
                    // текст уведомления на сайте (доступен html и бб-коды)
                    "NOTIFY_MESSAGE" => '[b]Внимание:[/b] получен новый заказ с сайта. Для просмотра перейдите по <a href="' . $TaskLink . '">ссылке</a>',
                );
                CIMNotify::Add($arMessageFields);
            }
        }


        /*
         // если планируете почту проверять вручную (в графе "проверять с периодом" почтового ящика стоит ноль) - то следующие 2 строчки нужно раскомментарить

         if (CModule::IncludeModule("socialnetwork"))
            CSocNetMessages::Add($arPortalMessageFields);
          */
    }


    /*if (mail("web@orfi.ru","CRM: test subject", "test body","From: smtp@orfi.ru"))
        echo "Почтовая система работает!";
    else
        echo "Неудача, почтовая система не работает, попробуйте еще!";*/
}

if(CModule::IncludeModule('im')){
    $arFieldsIm = array(
        "NOTIFY_TITLE" => "Сумма предоплаты меньше ". ''.'р.', //заголовок
        "MESSAGE" => 'Нельзя изменить стадию. Cумма предоплаты '.'' .'р. меньше '.''.'р. - 50% от суммы товаров со свойством "Батут"',
        "MESSAGE_TYPE" => IM_MESSAGE_SYSTEM, // IM_MESSAGE_PRIVATE, IM_MESSAGE_CHAT, IM_MESSAGE_OPEN, IM_MESSAGE_SYSTEM, IM_MESSAGE_OPEN_LINE
        "TO_USER_ID" => 1,
        "FROM_USER_ID" => 1,
        //"AUTHOR_ID" => $userIdOther, //может отличаться от FROM_USER_ID

        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,  // IM_NOTIFY_CONFIRM, IM_NOTIFY_SYSTEM, IM_NOTIFY_FROM
        "NOTIFY_MODULE" => "main", // module id sender (ex: xmpp, main, etc)
        "NOTIFY_EVENT" => "manage",
    );
    //CIMMessenger::Add($arFieldsIm);
}

