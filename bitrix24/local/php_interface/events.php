<?php

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(
    'disk',
    'onAfterAddFile',
    'showData'
);

$eventManager->addEventHandler(
    'crm',
    'OnBeforeCrmDealUpdate',
    'checkDeal'
);

function showData(\Bitrix\Main\Event $event)
{
    list($file) = $event->getParameters();
    //$arFile = print_r($file,true);
    //$strFile = '<pre>'.$arFile.'</pre>';
    $strFile = '';
    $urlManager = \Bitrix\Disk\Driver::getInstance()->getUrlManager();
    $strFile =  $urlManager->getPathFileDetail($file);
    // //company/personal/user/1/disk/file/slide_01_desktop.jpg
    // //workgroups/group/2/disk/file/slide_01_tablet.jpg

    if($file instanceof \Bitrix\Disk\File)
    {
        //$arFile = print_r($file,true);

        //$infoFile = $file->getFile();
        //$arFile = print_r($file,true);

        $arFile = $file->getMapAttributes();
        $arFile = print_r($arFile,true);
        // $strFile = '<pre>'.$arFile.'</pre>';
        //var_dump("Filename {$file->getName()}");


    }

    if(Loader::IncludeModule("im")) {
        $arMessageFields = array(
            "TO_USER_ID"     => 1, // получатель
            "FROM_USER_ID"   => 3, // отправитель (может быть >0)
            "NOTIFY_TYPE"    => IM_NOTIFY_SYSTEM, // тип уведомления
            "NOTIFY_MODULE"  => "tasks", // модуль запросивший отправку уведомления
            "NOTIFY_TAG"     => "FAX-TASKS", // символьный тэг для группировки (будет выведено только одно сообщение), если это не требуется - не задаем параметр
            // текст уведомления на сайте (доступен html и бб-коды)
            "NOTIFY_MESSAGE" => 'Данне файла: '.$strFile, //$file->getTypeFile()
        );
        CIMNotify::Add($arMessageFields);
    }
}

function checkDeal(&$arFields) {

    $out = print_r($arFields,true);
    if(CModule::IncludeModule('im')){

        $arFieldsIm = array(
            "NOTIFY_TITLE" => 'заголовок', //заголовок
            "MESSAGE" => $out,
            "MESSAGE_TYPE" => IM_MESSAGE_SYSTEM, // IM_MESSAGE_PRIVATE, IM_MESSAGE_CHAT, IM_MESSAGE_OPEN, IM_MESSAGE_SYSTEM, IM_MESSAGE_OPEN_LINE
            "TO_USER_ID" => 1,
            "FROM_USER_ID" => 1,
            //"AUTHOR_ID" => $userIdOther, //может отличаться от FROM_USER_ID

            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,  // IM_NOTIFY_CONFIRM, IM_NOTIFY_SYSTEM, IM_NOTIFY_FROM
            "NOTIFY_MODULE" => "main", // module id sender (ex: xmpp, main, etc)
            "NOTIFY_EVENT" => "manage",
        );
        CIMMessenger::Add($arFieldsIm);
        //$arFields['RESULT_MESSAGE'] = 'Текст ошибки1!';
        //$GLOBALS['APPLICATION']->ThrowException('Текст ошибки2!');
        //return false;
    }
}