<?php
namespace Bitrix\Sale;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Fields\Validators\LengthValidator;

/**
 * Class DiscountCouponTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> DISCOUNT_ID int mandatory
 * <li> ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> ACTIVE_FROM datetime optional
 * <li> ACTIVE_TO datetime optional
 * <li> COUPON string(32) mandatory
 * <li> TYPE int optional default 0
 * <li> MAX_USE int optional default 0
 * <li> USE_COUNT int optional default 0
 * <li> USER_ID int optional default 0
 * <li> DATE_APPLY datetime optional
 * <li> TIMESTAMP_X datetime optional
 * <li> MODIFIED_BY int optional
 * <li> DATE_CREATE datetime optional
 * <li> CREATED_BY int optional
 * <li> DESCRIPTION text optional
 * <li> DISCOUNT_ID reference to {@link \Bitrix\Sale\SaleDiscountTable}
 * <li> MODIFIED_BY reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Bitrix\Sale
 **/

class DiscountCouponTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_sale_discount_coupon';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            'ID' => new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_ID_FIELD'),
                ]
            ),
            'DISCOUNT_ID' => new IntegerField(
                'DISCOUNT_ID',
                [
                    'required' => true,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_DISCOUNT_ID_FIELD'),
                ]
            ),
            'ACTIVE' => new BooleanField(
                'ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_ACTIVE_FIELD'),
                ]
            ),
            'ACTIVE_FROM' => new DatetimeField(
                'ACTIVE_FROM',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_ACTIVE_FROM_FIELD'),
                ]
            ),
            'ACTIVE_TO' => new DatetimeField(
                'ACTIVE_TO',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_ACTIVE_TO_FIELD'),
                ]
            ),
            'COUPON' => new StringField(
                'COUPON',
                [
                    'required' => true,
                    'validation' => function()
                    {
                        return[
                            new LengthValidator(null, 32),
                        ];
                    },
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_COUPON_FIELD'),
                ]
            ),
            'TYPE' => new IntegerField(
                'TYPE',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_TYPE_FIELD'),
                ]
            ),
            'MAX_USE' => new IntegerField(
                'MAX_USE',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_MAX_USE_FIELD'),
                ]
            ),
            'USE_COUNT' => new IntegerField(
                'USE_COUNT',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_USE_COUNT_FIELD'),
                ]
            ),
            'USER_ID' => new IntegerField(
                'USER_ID',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_USER_ID_FIELD'),
                ]
            ),
            'DATE_APPLY' => new DatetimeField(
                'DATE_APPLY',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_DATE_APPLY_FIELD'),
                ]
            ),
            'TIMESTAMP_X' => new DatetimeField(
                'TIMESTAMP_X',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_TIMESTAMP_X_FIELD'),
                ]
            ),
            'MODIFIED_BY' => new IntegerField(
                'MODIFIED_BY',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_MODIFIED_BY_FIELD'),
                ]
            ),
            'DATE_CREATE' => new DatetimeField(
                'DATE_CREATE',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_DATE_CREATE_FIELD'),
                ]
            ),
            'CREATED_BY' => new IntegerField(
                'CREATED_BY',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_CREATED_BY_FIELD'),
                ]
            ),
            'DESCRIPTION' => new TextField(
                'DESCRIPTION',
                [
                    'title' => Loc::getMessage('DISCOUNT_COUPON_ENTITY_DESCRIPTION_FIELD'),
                ]
            ),
            'DISCOUNT' => new Reference(
                'DISCOUNT',
                '\Bitrix\Sale\SaleDiscount',
                ['=this.DISCOUNT_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            'USER' => new Reference(
                'USER',
                '\Bitrix\User\User',
                ['=this.MODIFIED_BY' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }
}