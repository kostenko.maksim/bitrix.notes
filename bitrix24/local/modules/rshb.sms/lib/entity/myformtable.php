<?php
/**
 * Class MyFormTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(255) optional
 * <li> SURNAME string(255) optional
 * <li> PHONE string(255) optional
 * <li> ACCESS int optional
 * </ul>
 *
 * @package Bitrix\Form
 **/
namespace Myname\Entity;

class MyFormTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'myname_form';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            'ID' => new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => 'ID',
                ]
            ),
            'NAME' => new StringField(
                'NAME',
                [
                    'required' => true,

                    'title' => 'Имя',
                ]
            ),
            'SURNAME' => new StringField(
                'SURNAME',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('RESULT_ANSWER_ENTITY_FORM_ID_FIELD'),
                ]
            ),
            'PHONE' => new IntegerField(
                'PHONE',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('RESULT_ANSWER_ENTITY_FIELD_ID_FIELD'),
                ]
            ),
            'ACCESS' => new IntegerField(
                'ACCESS',
                [
                    'title' => Loc::getMessage('RESULT_ANSWER_ENTITY_ANSWER_ID_FIELD'),
                ]
            ),
        ];
    }
}