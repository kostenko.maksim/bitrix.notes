<?php

namespace RSHB\Sms;


class SmsSender extends \Bitrix\MessageService\Sender\Base
{
    public const ID = 'rshbinsSms';

    public function getId(): string
    {
        return static::ID;
    }

    public function getName(): string
    {
        return 'SMS-сервис: РСХБ';
    }

    public function getShortName()
    {
        return $this->getName();
    }

    public function getFromList(): array
    {
        return [
            [
                'id' => 'rshbinsSms',
                'name' => 'РСХБ',
            ]
        ];
    }

    public function sendMessage(array $messageFieldsFields): \Bitrix\MessageService\Sender\Result\SendMessage
    {
        global $USER;
        $userID = $USER->GetID();
        $messageFieldsFields['MESSAGE_BODY'] = $this->prepareMessageBodyForSend($messageFieldsFields['MESSAGE_BODY']);
        //AddMessage2Log(prt($messageFieldsFields));

        $result = new \Bitrix\MessageService\Sender\Result\SendMessage();
        $result->setStatus(\Bitrix\MessageService\MessageStatus::DELIVERED);
        $result->setExternalId(uniqid());

        //$dialogId = \Bitrix\Main\Config\Option::get('messageservice', 'dummy_dialog_id', '');
        $dialogId = 'chat12';
        if (
            !empty($dialogId)
            && \Bitrix\Main\Loader::includeModule('im')
            //&& \Bitrix\Im\Common::isChatId($dialogId)
        )
        {
            $messageFieldsFields['provider_settings'] = [
                "socketTimeout" => $this->socketTimeout,
                "streamTimeout" => $this->streamTimeout,
            ];
            \CIMMessage::Add(array(
                'FROM_USER_ID' => 4,
                'TO_USER_ID' => $userID,
                'MESSAGE' => '[b]MessageService test message[/b] :idea: [br][br] <pre>' . print_r($messageFieldsFields, 1).'</pre>',
            ));

            /*\CIMChat::AddMessage([
                'DIALOG_ID' => $dialogId,
                'FROM_USER_ID' => 4,
                //'SYSTEM' => 'Y',
                'MESSAGE' => '[b]MessageService test message[/b] :idea: [br][br] <pre>' . print_r($messageFieldsFields, 1).'</pre>',
                //'MESSAGE' => '[b]MessageService test message[/b] :idea: [br][br]',
            ]);*/
        }

        return $result;
    }

    public function canUse(): bool
    {
        return true;
    }

    public static function onGetSmsSenders()
    {
        return [new self()];
    }

}