<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

const MODULE_ID_RSHB_SMS = 'rshb.sms';

if ($APPLICATION->GetGroupRight(MODULE_ID_RSHB_SMS)<"S")
{
    $APPLICATION->AuthForm(Loc::getMessage(MODULE_ID_RSHB_SMS));
}

\Bitrix\Main\Loader::includeModule('rshb.sms');


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций
$aTabs = [
    [
        'DIV' => 'edit1',
        'TAB' => "Основные настройки",
        'OPTIONS' => [
            [
                'RSHB_SMS_API_URL',
                "URL API",
                '',
                ['text', 150],
            ],
            [
                'RSHB_SMS_API_TOKEN',
                "Токен API",
                '',
                ['text', 150],
            ],
        ]
    ],
    [
        'DIV' => 'edit2',
        'TAB' => "Дополнительные настройки",
        'OPTIONS' => [
            [
                'RSHB_SMS_API_URL2',
                "URL API2",
                '',
                ['text', 150],
            ],
        ]
    ],
];
// Saving
if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption))
                continue;

            if ($arOption['note'])
                continue;

            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set(MODULE_ID_RSHB_SMS, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
}

// Output
$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<?php $tabControl->Begin(); ?>
<form method='post'
      action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>'
      name='MYNAME_MAIN_settings'>

    <?php
    foreach ($aTabs as $aTab):
        if($aTab['OPTIONS']):
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList(MODULE_ID_RSHB_SMS, $aTab['OPTIONS']);
        endif;
    endforeach; ?>

    <?php
    $tabControl->BeginNextTab();

    $tabControl->Buttons();
    ?>

    <input type="submit" name="Update" value="Сохранить">
    <input type="reset" name="reset" value="Сбросить">
    <?=bitrix_sessid_post();?>
</form>
<?php $tabControl->End(); ?>

