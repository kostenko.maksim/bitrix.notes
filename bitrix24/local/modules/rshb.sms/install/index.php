<?php
class rshb_sms extends CModule
{
    public $MODULE_ID           = 'rshb.sms';
    public $MODULE_NAME         = 'РСХБ: SMS-сервис';
    public $MODULE_DESCRIPTION  = 'Отправка SMS используя шлюз компании';
    public $PARTNER_NAME        = 'РСХБ';
    public $PARTNER_URI         = 'https://rshbins.ru';

    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    /**
     * Основные параметры модуля
     */
    public function __construct()
    {
        include __DIR__ . '/version.php';
        /** @var array $arModuleVersion */

        $this->MODULE_VERSION       = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE  = $arModuleVersion['VERSION_DATE'];
    }

    /**
     * Установка модуля
     */
    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences(
            'messageservice',
            'onGetSmsSenders',
            $this->MODULE_ID,
            'RSHB\Sms\SmsSender',
            'onGetSmsSenders'
        );
    }

    /**
     * Удаление модуля
     */
    function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);
        UnRegisterModuleDependences(
            'messageservice',
            'onGetSmsSenders',
            $this->MODULE_ID,
            'RSHB\Sms\SmsSender',
            'onGetSmsSenders'
        );
    }
}
