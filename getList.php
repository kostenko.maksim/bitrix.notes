<?php
/**
 * Различные выборки
 **/


/**
 * Задача: провести аудит свойств инфоблока и найти неиспользуемые
 * имеем:
 *  - инфоблок типа 2.0
 *  - значения свойств хранятся: в отдельной таблице для данного информационного блока
 *  - кол-во свойств 1010
 *  - таблица b_iblock_element_prop_s91 имеет тип InnoDB (у которой ограничение на кол-во столбцов 1000)
 *
 * при очередном добавлении нового свойства можно словить ошибку MySQL
 * Ошибка ([14] ALTER TABLE b_iblock_element_prop_s91 ADD PROPERTY_1436 numeric(18,4))
 * что нам говорит что мы уперлись в ограничение типа таблицы InnoDB
 * пока принято решение удалить неиспользуемые свойства инфоблока
 */
$iblockID = 91;
$arFilter = ['IBLOCK_ID' => $iblockID];
$arProps = [];
$properties = CIBlockProperty::GetList(array(), $arFilter);
while ($propFields = $properties->GetNext()) {
    $arProps[$propFields['ID']] = $propFields['NAME'];
}
echo 'Кол-во свойств инфоблока: '.count($arProps); // 1010

$emptyProps = [];
foreach ($arProps as $propertyId => $propertyName) {
    $arElement = [];
    $arFilter = ['IBLOCK_ID' => $iblockID];
    $arSelect = ['ID'];
    $arSelect[] = 'PROPERTY_' . $propertyId;
    // выбираем только заполненные значения
    $arFilter['!PROPERTY_' . $propertyId] = false;
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()) {
        $arElement[] = $ob->GetFields();
    }

    // если не нашли заполненных свойств
    if(empty($arElement))
        $emptyProps[$propertyId] = $propertyName;

}
echo 'Кол-во неиспользуемых свойств: '.count($emptyProps),PHP_EOL;
print_r($emptyProps);

//проверить SQL-запросом, подставив нужный ID
//SELECT COUNT(*) AS CNT FROM b_iblock_element_prop_s91 WHERE PROPERTY_1430 is NULL OR PROPERTY_1430 = 0 OR PROPERTY_1430 = "";
// должно выплюнуть значение общего кол-ва строк в таблице b_iblock_element_prop_s91









