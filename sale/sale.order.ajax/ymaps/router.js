ymaps.ready(init);

function init() {
    let div = document.createElement('div');
    div.id = 'map';
    document.body.append(div);

    var myMap = new ymaps.Map("map", {
        center: [55.745508, 37.435225],
        zoom: 10,

    });

    var addressDelivery = 'Москва, Кольцевая линия, метро Новослободская';
    var arrSklad = [
        'г. Москва, ТРЦ «Новомосковский», ул. Хабарова, д. 2',
        'г. Москва, ТРЦ "Саларис", Киевское ш., д. 1',
        'г. Москва, ТРЦ "Павелецкая Плаза", Павелецкая, д.3',
        'г. Москва, ТРЦ "Щелковский", Щелковское шоссе, 75',
    ]
    arrSklad.forEach(function(item, i, arr) {
        ymaps.route([
            item,
            addressDelivery
        ]).then(function (route) {
            console.log(route)
            console.log(item);
            let balloonText = route.properties._data.RouterRouteMetaData.Length.text+' - '+route.properties._data.RouterRouteMetaData.JamsTime.text;
            console.log(balloonText);
            var points = route.getWayPoints(),
                lastPoint = points.getLength() - 1;
            points.get(lastPoint).properties.set({
                iconCaption: 'Адрес доставки'
            });
            points.get(0).properties.set({
                iconCaption: balloonText
            });

            myMap.geoObjects.add(route);


        }, function (error) {
            alert('Возникла ошибка: ' + error.message);
        });
    });
}