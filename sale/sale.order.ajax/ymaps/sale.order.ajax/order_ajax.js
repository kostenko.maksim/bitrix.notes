BX.namespace('BX.Sale.OrderAjaxComponent');

(function() {
    'use strict';

    /**
     * Show empty default property value to multiple properties without default values
     */
    if (BX.Sale && BX.Sale.Input && BX.Sale.Input.Utils) {
        BX.Sale.Input.Utils.asMultiple = function(value) {
            if (value === undefined || value === null || value === '') {
                return [];
            } else if (value.constructor === Array) {
                var i = 0,
                    length = value.length,
                    val;

                for (; i < length;) {
                    val = value[i];

                    if (val === undefined || val === null || val === '') {
                        value.splice(i, 1);
                        --length;
                    } else {
                        ++i;
                    }
                }

                return value.length ? value : [''];
            } else {
                return [value];
            }
        };
    }

    BX.Sale.OrderAjaxComponent = {
        orderSaveAllowed: false,
        socServiceHiddenNode: false,

        locationsCompletion: function() {
            var i, locationNode, clearButton, inputStep, inputSearch,
                arProperty, data, section;

            this.locationsInitialized = true;
            this.fixLocationsStyle(this.deliveryBlockNode, this.deliveryHiddenBlockNode);
            this.fixLocationsStyle(this.propsBlockNode, this.propsHiddenBlockNode);

            var $room = $("#soa-property-10");

            if ($room.length > 0 && !$room.val().length && this.lastDeliveryRoom)
                $room.val(this.lastDeliveryRoom);


            for (i in this.locations) {
                if (!this.locations.hasOwnProperty(i))
                    continue;

                locationNode = this.orderBlockNode.querySelector('div[data-property-id-row="' + i + '"]');
                if (!locationNode)
                    continue;

                clearButton = locationNode.querySelector('div.bx-ui-sls-clear');
                inputStep = locationNode.querySelector('div.bx-ui-slst-pool');
                inputSearch = locationNode.querySelector('input.bx-ui-sls-fake[type=text]');

                locationNode.removeAttribute('style');
                this.bindValidation(i, locationNode);
                if (clearButton) {
                    BX.bind(clearButton, 'click', function(e) {
                        var target = e.target || e.srcElement,
                            parent = BX.findParent(target, { tagName: 'DIV', className: 'form-group' }),
                            locationInput;

                        if (parent)
                            locationInput = parent.querySelector('input.bx-ui-sls-fake[type=text]');

                        if (locationInput)
                            BX.fireEvent(locationInput, 'keyup');
                    });
                }

                if (!this.firstLoad && this.options.propertyValidation) {
                    if (inputStep) {
                        arProperty = this.validation.properties[i];
                        data = this.getValidationData(arProperty, locationNode);
                        section = BX.findParent(locationNode, { className: 'bx-soa-section' });

                        if (section && section.getAttribute('data-visited') == 'true')
                            this.isValidProperty(data);
                    }

                    if (inputSearch)
                        BX.fireEvent(inputSearch, 'keyup');
                }


                var $street = $("#soa-property-8");// ФЛ
                var $house = $("#soa-property-9");// ФЛ

                if($street && $street.length === 0)
                    $street = $("#soa-property-31");//ЮЛ

                if($house && $house.length === 0)
                    $house = $("#soa-property-32");//ЮЛ

                var _this = this;

                // var locationCity = $('input.bx-ui-sls-fake').attr('title');
                var locationCity = inputSearch.title;

                if (locationCity.hasOwnProperty('length') && locationCity.length && $street.length > 0) {
                    locationCity = locationCity.split(',');

                    if (locationCity[0].hasOwnProperty('length') && locationCity[0].length) {

                        let selectAddress,
                            selectHouse;

                        _this.deliveryAddressCity = locationCity[0];

                        // улица
                        $street.suggestions({
                            token: token,
                            type: "ADDRESS",
                            hint: false,
                            bounds: "street",
                            triggerSelectOnBlur: true,
                            triggerSelectOnEnter: true,
                            triggerSelectOnSpace: true,
                            constraints: [{
                                locations: { city: locationCity[0] },
                                label: "",
                                deletable: true
                            }],
                            onSelect: function(suggestion, changed) {
                                if (changed) {
                                    selectAddress = suggestion.value;
                                    _this.deliveryAddressStreet = suggestion.value;
                                    _this.disabledInput($street);
                                    $house.removeAttr('readonly');
                                    $house.removeClass('bx-ui-input-clear');
                                }
                            },
                            onSelectNothing: function (query) {
                                //if (query != selectAddress)
                                //  $street.val(selectAddress);
                            },
                            beforeRender: function (container) {
                                // если input задисейблен то скроем подсказки
                                container[0].hidden = $(this).hasClass('bx-ui-input-clear');
                            }
                        });

                        // дом
                        $house.suggestions({
                            token: token,
                            type: "ADDRESS",
                            hint: false,
                            noSuggestionsHint: false,
                            bounds: "house",
                            constraints: $street,
                            onSelect: function(suggestion, changed) {
                                if (changed) {
                                    selectHouse = suggestion.value;
                                    _this.deliveryAddressHouse = suggestion.value;
                                    _this.disabledInput($house);
                                    _this.disabledInput($street);
                                    // если выбрали доставку экспресс проставим рекомендованный склад
                                    let selectedID = $('.bx-soa-pp-company.section-box.bx-selected').find('input[name="DELIVERY_ID"]').val();
                                    if (_this.expressDeliveryID.includes(Number(selectedID)))
                                        _this.getRecommendPickUp();
                                }
                            },
                            onSelectNothing: function(query) {
                                //if (query != selectHouse)
                                //  $house.val(selectHouse);
                            },
                            beforeRender: function (container) {
                                // если input задисейблен то скроем подсказки
                                container[0].hidden = $(this).hasClass('bx-ui-input-clear');
                            }
                        });



                    }
                }
            }
            if (this.firstLoad && this.result.IS_AUTHORIZED && typeof this.result.LAST_ORDER_DATA.FAIL === 'undefined') {
                this.showActualBlock();
            } else if (!this.result.SHOW_AUTH) {
                this.changeVisibleContent();
            }

            this.checkNotifications();


            /**
             *  Получаем данные по ИНН
             * @type {*|jQuery|HTMLElement}
             */
            let $inn = $("#soa-property-49");
            if ($inn.hasOwnProperty('length') && $inn.length > 0) {

                let $name = $("#soa-property-29"),
                    $kpp = $("#soa-property-50"),
                    $bik = $("#soa-property-53"),
                    $address = $("#soa-property-51");

                function formatSelected(suggestion) {
                    let data = suggestion.data,
                        kpp = data.kpp ? data.kpp : '';

                    $inn.val(data.inn);
                    $name.val(data.name.short_with_opf);
                    $kpp.val(kpp);
                    $address.val(data.address.unrestricted_value);

                    return data.inn || '';
                }

                $inn.suggestions({
                    token: token,
                    type: "PARTY",
                    params: {
                        status: ["ACTIVE"]
                    },
                    count: 5,
                    // начинаем показывать Подсказки только с 4 символа
                    minChars: 4,
                    formatSelected: formatSelected
                });
            }
        },

        getPickUpInfoArray: function(storeIds) {
            if (!storeIds || storeIds.length <= 0)
                return [];

            var arr = [],
                i;

            /*for (i = 0; i < storeIds.length; i++) {
                if (this.result.STORE_LIST[storeIds[i]])
                    arr.push(this.result.STORE_LIST[storeIds[i]]);
            }*/

            let arrStoreList = Object.values(this.result.STORE_LIST);
            // сортируем список складов по убыванию остатков на них
            arrStoreList.sort((a, b) => Number(a.AMOUNT) < Number(b.AMOUNT) ? 1 : -1);


            // для склада встройка получим магазины розницы не учитывая остатки на них
            if (this.getSelectedDelivery().ID == this.builtInDeliveryID)
                arrStoreList = this.result.STORE_LIST_RETAIL;

            return arrStoreList;
        },

        /**
         * Метод выбирает ID рекомендованного склада
         * если заполнен адрес доставки то выбирается склад с кратчайшим маршрутом
         * иначе с наибольшим остатком
         * и проставляет значение BUYER_STORE
         * @returns {number}
         */
        getRecommendPickUp: function() {

            let pickUpInput = BX('BUYER_STORE'),
                recommendPickUpList = this.getPickUpInfoArray([0]),// передаем любой массив, теперь не имеет значение
                recommendPickUpId = 0;

            if (pickUpInput && recommendPickUpList.length > 0) {
                //проверяем заполнены ли свойства адреса и если да то выбираем кратчайший маршрут
                let addressDelivery;
                let shortestRoutePickUpId;

                if (this.deliveryAddressCity != '' && this.deliveryAddressStreet != '' && this.deliveryAddressHouse != '') {
                    addressDelivery = this.deliveryAddressCity +', '+ this.deliveryAddressStreet +', '+ this.deliveryAddressHouse;
                }

                this.Maps.getShortestRoutePickUp(recommendPickUpList,addressDelivery)
                    .then(function (response) {
                    shortestRoutePickUpId = response;

                    recommendPickUpId = shortestRoutePickUpId ?? recommendPickUpList[0].ID;
                    pickUpInput.setAttribute('value', recommendPickUpId);
                    })
                    .catch(error => console.log(error));
            }
        },


    };

})();
