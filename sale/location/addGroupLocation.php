<?php
/**
* Задача: создать группу местоположений и включить в нее все местоположения с типом "Село"/"VILLAGE"
*/

use Bitrix\Sale\Location\GroupTable;
use Bitrix\Sale\Location\GroupLocationTable;
use Bitrix\Sale\Location\LocationTable;
use Bitrix\Sale\Location\TypeTable;
use Bitrix\Sale\Location\Admin\GroupHelper;

$groupNameRU = 'Села';
$groupNameEN = 'Village';

$data = [
    'CODE' => 'VILLAGE',
    'NAME' => $groupNameRU,
    'NAME_RU' => $groupNameRU,
    'NAME_EN' => $groupNameEN,
];

// Создаем новую группу местоположений
$newGroup = GroupHelper::add($data);

if ($newGroup['success']) {
    $groupId = $newGroup['id'];

    // Получаем все местоположения типа "Село"
    $filter = array(
        'CODE' => 'VILLAGE' // код типа местоположения для "Село"
    );
    $typeId = TypeTable::getList(array('filter' => $filter))->fetch()['ID'];
    $filter = array(
        'TYPE_ID' => $typeId
    );
    $locations = LocationTable::getList(array('filter' => $filter, 'select' => array('ID')))->fetchAll();

    // Добавляем все местоположения типа "Село" в созданную группу
    foreach ($locations as $location) {
        $groupLocation = GroupLocationTable::add(array(
            'LOCATION_GROUP_ID' => $groupId,
            'LOCATION_ID' => $location['ID']
        ));
        if (!$groupLocation->isSuccess()) {
            var_dump($groupLocation->getErrors()); // выводим ошибки при добавлении группы
        }
    }

    echo "Group created with ID: ".$groupId;
} else {
    var_dump($newGroup); // выводим ошибки при создании группы
}



