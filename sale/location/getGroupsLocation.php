<?php
/**
 * Задача: получить список групп местоположений
 */

function getGroupsLocation(): array
{
    $arGroupLocation = [];
    //TODO: прикрутить тегированный кеш
    // нужно вешать очистку кэша на OnLocationGroupAdd, OnLocationGroupDelete, OnLocationGroupUpdate
    $obCache = new \CPHPCache();
    $cacheID = SITE_ID.'_getList_GroupLocation';
    if ($obCache->InitCache(0, $cacheID , '/'.$cacheID )) {//кэш пока отключим
        $arGroupLocation = $obCache->GetVars();
    } elseif ($obCache->StartDataCache()) {
        $res = \Bitrix\Sale\Location\GroupLocationTable::getList([
            'select' => [
                'LOCATION_GROUP_ID',
                'NAME_RU' => 'NAME__RU.NAME',
            ],
            'runtime' => [
                'NAME__RU' => [
                    'data_type' => 'Bitrix\Sale\Location\Name\Group',
                    'reference' => [
                        '=this.LOCATION_GROUP_ID' => 'ref.LOCATION_GROUP_ID',
                        '=ref.LID' => ['?', 'ru']
                    ],
                    'join_type' => 'left'
                ],
            ],
        ]);
        while($groupLocation = $res->fetch()) {
            $arGroupLocation[$groupLocation['LOCATION_GROUP_ID']] = [
                'CODE' => $groupLocation['LOCATION_GROUP_ID'],
                'NAME' => $groupLocation['NAME_RU'],
            ];
        }
        $obCache->EndDataCache($arGroupLocation);
    }

    return $arGroupLocation;

}