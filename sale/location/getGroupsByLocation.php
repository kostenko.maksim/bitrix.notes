<?php
/**
 * Задача: получить список групп местоположений в которой состоит местоположение (по CODE)
 */

function getGroupsByLocation($locationCode): array
{
    $res = \Bitrix\Sale\Location\LocationTable::getList([
        'filter' => ['=CODE' => $locationCode],
        'select' => [
            'ID',
            'LEFT_MARGIN',
            'RIGHT_MARGIN'
        ]
    ]);
    if (!$loc = $res->fetch()) {
        return [];
    }
    $locations = [$loc['ID']];
    $res = \Bitrix\Sale\Location\LocationTable::getList([
        'filter' => [
            '<LEFT_MARGIN' => $loc['LEFT_MARGIN'],
            '>RIGHT_MARGIN' => $loc['RIGHT_MARGIN'],
            'NAME.LANGUAGE_ID' => LANGUAGE_ID,
        ],
        'select' => [
            'ID',
            'LOCATION_NAME' => 'NAME.NAME'
        ]
    ]);
    while ($locParent = $res->fetch()) {
        $locations[] = $locParent['ID'];
    }
    $res = \Bitrix\Sale\Location\GroupLocationTable::getList([
        'filter' => ['=LOCATION_ID' => $locations]
    ]);
    $groups = [];
    while ($groupLocation = $res->fetch()) {
        $groups[] = $groupLocation['LOCATION_GROUP_ID'];
    }
    return $groups;
}