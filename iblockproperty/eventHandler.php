<?php
/**
 * Регистрируем наши кастомные типы свойств для инфоблоков
 * bitrix/php_interface/site_id/init.php
 */
// Пользовательское свойство группы местоположений
AddEventHandler(
    "iblock",
    "OnIBlockPropertyBuildList",
    [\Custom\Main\IBlock\CustomProperty\IBlockPropertyGroupLocation::class, 'GetUserTypeDescription']
);