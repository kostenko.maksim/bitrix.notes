<?php

namespace Custom\Main\IBlock\CustomProperty;


/**
 * Создает пользовательский тип выбора группы местоположения
 *
 * @package Custom\Main\IBlock\CustomProperty
 */
class IBlockPropertyGroupLocation
{

    const PROP_TYPE_INTEGER  = 'N';

    /**
     * В базе храним только число - сам идентификатор группы
     *
     * @return string
     */
    public static function getBasePropertyType(): string
    {
        return self::PROP_TYPE_INTEGER;
    }

    /**
     * Уникальный код свойства привязки к группе местоположения
     *
     * @return string
     */
    public static function getCustomPropertyType(): string
    {
        return 'group_location_code';
    }

    /**
     * Название свойства для вывода в админке
     *
     * @return string
     */
    public static function getTitle(): string
    {
        return 'Группа местоположений';
    }

    /**
     * @inheritDoc
     */
    public static function getPropertyFieldHtml(array $prop_data, array $prop_value, array $control_names): string
    {
        $form_select = '<select name="' . $control_names['VALUE'] . '" id="' . $control_names['VALUE'] . '">';
        $arGroupLocation = self::getGroupsLocation();
        if (!empty($arGroupLocation)) {
            foreach ($arGroupLocation as $key => $element) {
                $name = $element['CODE'] ? '['.$element['CODE'].'] '.$element['NAME'] : $element['NAME'];
                $form_select .= sprintf(
                    '<option value="%s"%s>%s</option>',
                    $element['CODE'],
                    ($element['CODE'] == $prop_value['VALUE']) ? ' selected' : '',
                    $name
                );
            }
        }
        $form_select .= '</select>';

        return $form_select;
    }

    public static function getAdminListViewHtml(array $prop_data, array $prop_value, array $control_name): string
    {
        if (!is_array($prop_value) || empty($prop_value["VALUE"]))
            return '';

        $arGroupLocation = self::getGroupsLocation();
        return $arGroupLocation[$prop_value['VALUE']]['NAME'] ?? 'error value: '.$prop_value['VALUE'];
    }

    /**
     * Стандартное описание пользовательского свойства
     *
     * @return array
     */
    public static function GetUserTypeDescription(): array
    {
        return [
            'PROPERTY_TYPE'         => static::getBasePropertyType(),
            'USER_TYPE'             => static::getCustomPropertyType(),
            'DESCRIPTION'           => static::getTitle(),
            'GetPropertyFieldHtml'  => [get_called_class(), 'getPropertyFieldHtml'],
            'GetAdminListViewHTML'  => [get_called_class(), 'getAdminListViewHtml'],
        ];
    }

    protected static function getGroupsLocation(): array
    {
        $arGroupLocation[] = [
            'CODE' => false,
            'NAME' => '(не установлено)',
        ];
        //TODO: прикрутить тегированный кеш
        // нужно вешать очистку кэша на OnLocationGroupAdd, OnLocationGroupDelete, OnLocationGroupUpdate
        $obCache = new \CPHPCache();
        $cacheID = SITE_ID.'_getList_GroupLocation';
        if ($obCache->InitCache(0, $cacheID , '/'.$cacheID )) {//кэш пока отключим
            $arGroupLocation = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            $res = \Bitrix\Sale\Location\GroupLocationTable::getList([
                'select' => [
                    'LOCATION_GROUP_ID',
                    'NAME_RU' => 'NAME__RU.NAME',
                ],
                'runtime' => [
                    'NAME__RU' => [
                        'data_type' => 'Bitrix\Sale\Location\Name\Group',
                        'reference' => [
                            '=this.LOCATION_GROUP_ID' => 'ref.LOCATION_GROUP_ID',
                            '=ref.LID' => ['?', 'ru']
                        ],
                        'join_type' => 'left'
                    ],
                ],
            ]);
            while($groupLocation = $res->fetch()) {
                $arGroupLocation[$groupLocation['LOCATION_GROUP_ID']] = [
                    'CODE' => $groupLocation['LOCATION_GROUP_ID'],
                    'NAME' => $groupLocation['NAME_RU'],
                ];
            }
            $obCache->EndDataCache($arGroupLocation);
        }

        return $arGroupLocation;

    }

    static function getGroupsByLocation($locationCode): array
    {
        $res = \Bitrix\Sale\Location\LocationTable::getList([
            'filter' => ['=CODE' => $locationCode],
            'select' => [
                'ID',
                'LEFT_MARGIN',
                'RIGHT_MARGIN'
            ]
        ]);
        if (!$loc = $res->fetch()) {
            return [];
        }
        $locations = [$loc['ID']];
        $res = \Bitrix\Sale\Location\LocationTable::getList([
            'filter' => [
                '<LEFT_MARGIN' => $loc['LEFT_MARGIN'],
                '>RIGHT_MARGIN' => $loc['RIGHT_MARGIN'],
                'NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ],
            'select' => [
                'ID',
                'LOCATION_NAME' => 'NAME.NAME'
            ]
        ]);
        while ($locParent = $res->fetch()) {
            $locations[] = $locParent['ID'];
        }
        $res = \Bitrix\Sale\Location\GroupLocationTable::getList([
            'filter' => ['=LOCATION_ID' => $locations]
        ]);
        $groups = [];
        while ($groupLocation = $res->fetch()) {
            $groups[] = $groupLocation['LOCATION_GROUP_ID'];
        }
        return $groups;
    }
}