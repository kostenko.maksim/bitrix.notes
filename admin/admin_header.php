<?php
/**
 * Делаем любой select в админке удобным с помощью плагина Select2
 * Bitrix будет искать этот файл по пути /local/php_interface/admin_header.php
 */


/** @global CMain $APPLICATION */

// Категории -> Редактировать -> Вкладка "Свойства элементов"
// Форма "Настройки отображения свойств элементов" -> активируем плагин Select2 для выпадающего списка
// если мы на странице редактирование категории активируем
$arCurPage = explode('/', $APPLICATION->GetCurPage());
$scriptEdit = end($arCurPage);
$catalogID = 1;

if ( ($scriptEdit === 'cat_section_edit.php' || $scriptEdit === 'iblock_section_edit.php') &&
    (int)$_REQUEST['IBLOCK_ID'] === $catalogID && $_REQUEST['from'] === 'iblock_section_admin' &&
    !empty($_REQUEST['ID']) // исключим создание новой категории
):

    CJSCore::Init( 'jquery' );
    // подключаем плагин Select2
    $APPLICATION->AddHeadScript(MARKUP_PATH . '/js/plugins/select2.full.min.js');
    ?>
    <script>
        BX.ready(function(){
            let select_SECTION_PROPERTY = $('#select_SECTION_PROPERTY');
            if(select_SECTION_PROPERTY && select_SECTION_PROPERTY.hasOwnProperty('length'))
                if (typeof select_SECTION_PROPERTY.select2 === 'function') {
                    select_SECTION_PROPERTY.select2({
                        minimumResultsForSearch: 10,
                        width: 'resolve'
                    });
                }
            $('.select2-container').css('width', '700px');
        });
    </script>
<?php endif;?>