<?php
/**
 * Задача: найти дубли пользователей у которых одинаковый мобильный телефон PERSONAL_PHONE
 * данный скрипт:
 * - выведет массивы дублей на страницу
 * - сохранит в CSV файл
 * - допом делает отбор пользователей авторизовавшихся последний год
 */


echo '<pre>';
use \Bitrix\Main\Application;

if (empty($_SERVER['DOCUMENT_ROOT']))
    $_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
$start = microtime(true);

if (!\Bitrix\Main\Engine\CurrentUser::get()->isAdmin())
    die('Access deny!');

$objCurrentDate = \Bitrix\Main\Type\DateTime::createFromTimestamp(strtotime('-1 year'));

$result = \Bitrix\Main\UserTable::getList(array(
    'filter' => array(
        'ACTIVE' => 'Y',
        '!=PERSONAL_PHONE' => false,
        '>=LAST_LOGIN' => $objCurrentDate,
    ),
    'select' => array('ACTIVE','ID','NAME','SECOND_NAME', 'LAST_NAME', 'LOGIN', 'PERSONAL_PHONE', 'PERSONAL_MOBILE', 'EMAIL','LAST_LOGIN'),
    'order' => array('PERSONAL_PHONE' => 'DESC'),
));

//$arUsers[] = ['ACTIVE','ID','NAME', 'LOGIN', 'PERSONAL_PHONE', 'PERSONAL_MOBILE', 'EMAIL','LAST_LOGIN'];
$arUsers = [];
while ($arUser = $result->fetch()) {
    $arUser['NAME'] = [$arUser['NAME'],$arUser['SECOND_NAME'],$arUser['LAST_NAME']];
    $arUser['NAME'] = implode(' ',$arUser['NAME']);
    unset($arUser['SECOND_NAME']);
    unset($arUser['LAST_NAME']);

    if (!empty($arUser['LAST_LOGIN']))
        $arUser['LAST_LOGIN'] = $arUser['LAST_LOGIN']->toString();

    $arUsers[$arUser['PERSONAL_PHONE']][] = $arUser;
}


print_r('Последняя авторизация от => '.$objCurrentDate->toString());
print_r('Всего пользователей => '. count($arUsers));


$arUserDuplicate = [];
$count = 0;
foreach ($arUsers as $phone => $user) {

    if (count($user) > 1) {
        $arUserDuplicate[$phone] = $user;
        $count++;
    }

}

print_r('Дублей => '. count($arUserDuplicate));
print_r($arUserDuplicate);

$arTitle['TITLE'][] = ['ACTIVE','ID','NAME', 'LOGIN', 'PERSONAL_PHONE', 'PERSONAL_MOBILE', 'EMAIL','LAST_LOGIN'];
$arUserDuplicate = $arTitle + $arUserDuplicate;

$fp = fopen($_SERVER['DOCUMENT_ROOT'].'/users2.csv', 'w');
fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // BOM

foreach ($arUserDuplicate as $userList) {
    foreach ($userList as $user) {
        fputcsv($fp, $user);
    }
}
fclose($fp);

echo '==============================================================='.PHP_EOL;
$resTime = (microtime(true) - $start);
echo 'Время выполнения скрипта: ' . $resTime . ' sec.'.PHP_EOL;
echo 'Время выполнения скрипта: ' . floor($resTime / 60) . ' min.'.PHP_EOL;
echo '</pre>';
