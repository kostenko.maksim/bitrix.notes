# Заметки по Веб-окружению - Linux (BitrixEnv)

## Выставить права на папки и файлы Bitrix:
```shell
chown -R bitrix:bitrix www
cd ~/www
find . -type d -exec chmod 775 {} \;
find . -type f -exec chmod 664 {} \;
```
## HDD/SSD замеры скорости
Выполните следующую команду для определения скорости ЗАПИСИ на накопитель:
```shell
dd if=/dev/zero of=tempfile bs=1M count=1024; sync
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 0.707486 s, 1.5 GB/s
```

Файл tempfile, сгенерированный предыдущей командой, был закэширован в буфер и скорость его чтения будет намного выше чем реальная скорость чтения непосредственно с жесткого диска.
Очистите кэш и измерьте реальную СКОРОСТЬ чтения непосредственно с жесткого диска:

```shell
$ /sbin/sysctl -w vm.drop_caches=3
vm.drop_caches = 3
$ dd if=tempfile of=/dev/null bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 1.35443 s, 793 MB/s
```
