<?php
/**
 * Проверка работы кеширования через Memcached
 * echo 'Проверка очистки кеша:<br>';
 *   $memcached = new Memcached();
 *   $memcached->addServer('/var/run/memcached/memcached.sock', 0);
 *   $memcached->flush();
 *   echo 'Кеш очищен:<br>';
 */
echo '<pre>';
echo 'Ищем расширение php-memcached',PHP_EOL;
// Class Memcached
if (!class_exists("Memcached")) {
    echo 'расширение php-memcached не установлено',PHP_EOL;
} else {
    echo 'расширение php-memcached установлено',PHP_EOL;
    $memcached = new Memcached();
    $memcached->addServer('/var/run/memcached/memcached.sock', 0); // Not to be confused with Memcache that use 'unix:///path/to/socket'
    $version = $memcached->getVersion();
    echo 'Server version: '.'<br>';
    print_r($version);
    $cacheResult = $memcached->get('keyMemcached');
    if (!empty($cacheResult)) {
        echo 'Данные из кеша Memcached (php-memcached):<br>';
        var_dump($cacheResult);
    } else {
        $memcached->set('keyMemcached', 'Какие-то данные', 10);
        echo 'Записываем данные в кеш Memcached $keyMemcached (будут храниться 10 секунд)','<br>';
        echo 'Обновите страницу для проверки';
    }
    echo '<br><br><br>';
}


echo 'Ищем расширение php-memcache',PHP_EOL;
// Class Memcache
if (!class_exists("Memcache")) {
    echo 'расширение php-memcache не установлено',PHP_EOL;
} else {
    $memcache = new Memcache;
    $memcache->connect('unix:///var/run/memcached/memcached.sock', 0) or exit("Невозможно подключиться к серверу Memcached");
    $version = $memcache->getVersion();
    echo 'Server version: '.$version.'<br>';

    $tmp_object = new stdClass;
    $tmp_object->str_attr = 'test';
    $tmp_object->int_attr = 123;

    $cacheResult = $memcache->get('keyMemcache');
    if (!empty($cacheResult)) {
        echo 'Данные из кеша Memcached (php-memcache):<br>';
        var_dump($cacheResult);
    } else {
        $memcache->set('keyMemcache', $tmp_object, false, 10) or die ("Не получилось оставить запись в Memcached");
        echo 'Записываем данные в кеш Memcached $keyMemcache (будут храниться 10 секунд)','<br>';
        echo 'Обновите страницу для проверки';
    }

}
echo '<br><br><br>';

// проверка работы кеширования
echo 'Проверка CPhpCache (Bitrix) - класс для кеширования PHP переменных и HTML результата выполнения скрипта:',PHP_EOL;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$cache = new CPHPCache();
if ($cache->InitCache(3600, '12356356gt' , '/' )) {
    $res = $cache->GetVars();
    $arResult = $res['arResult'];
    print_r($arResult);
    echo ' - данные получены из CACHE';
} elseif ($cache->StartDataCache()) {
    $arResult = array(1,2,3,4,5,6);
    $cache->EndDataCache(array("arResult" => $arResult));
    print_r($arResult);
    echo ' - данные НЕ из CACHE, попробуйте обновить страницу';
}

echo '</pre>';