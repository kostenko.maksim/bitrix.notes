<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

const MODULE_ID_MYNAME_MAIN = 'myname.main';

if ($APPLICATION->GetGroupRight(MODULE_ID_MYNAME_MAIN)<"S")
{
    $APPLICATION->AuthForm(Loc::getMessage(MODULE_ID_MYNAME_MAIN));
}

\Bitrix\Main\Loader::includeModule('myname.main');


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций
$aTabs = [
    [
        'DIV' => 'edit1',
        'TAB' => "Основные настройки",
        'OPTIONS' => [
            [
                'CONFIG_ID_1',
                "Настройка 1",
                '',
                ['text', 50],
            ],
        ]
    ],
    [
        'DIV' => 'edit2',
        'TAB' => "Доп. настройки",
        'OPTIONS' => [
            [
                'CONFIG_ID_2',
                "Настройка 2",
                '',
                ['text', 50],
            ],
            [
                'DEFAULT_KEY',
                "API-Key",
                'default_key',
                ['text', 150],
            ],
        ]
    ],
];
// Saving
if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption))
                continue;

            if ($arOption['note'])
                continue;

            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set(MODULE_ID_MYNAME_MAIN, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
}

// Output
$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<?php $tabControl->Begin(); ?>
<form method='post'
      action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>'
      name='POLARIS_MAIN_settings'>

    <?php
    foreach ($aTabs as $aTab):
        if($aTab['OPTIONS']):
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList(MODULE_ID_MYNAME_MAIN, $aTab['OPTIONS']);
        endif;
    endforeach; ?>

    <?php
    $tabControl->BeginNextTab();

    $tabControl->Buttons();
    ?>

    <input type="submit" name="Update" value="Сохранить">
    <input type="reset" name="reset" value="Сбросить">
    <?=bitrix_sessid_post();?>
</form>
<?php $tabControl->End(); ?>

