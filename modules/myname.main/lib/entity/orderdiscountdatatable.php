<?php
namespace Bitrix\Sale;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Fields\Validators\LengthValidator;

/**
 * Class OrderDiscountDataTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ORDER_ID int mandatory
 * <li> ENTITY_TYPE int mandatory
 * <li> ENTITY_ID int mandatory
 * <li> ENTITY_VALUE string(255) optional
 * <li> ENTITY_DATA text mandatory
 * <li> ORDER_ID reference to {@link \Bitrix\Sale\SaleOrderTable}
 * </ul>
 *
 * @package Bitrix\Sale
 **/

class OrderDiscountDataTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_sale_order_discount_data';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            'ID' => new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ID_FIELD'),
                ]
            ),
            'ORDER_ID' => new IntegerField(
                'ORDER_ID',
                [
                    'required' => true,
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ORDER_ID_FIELD'),
                ]
            ),
            'ENTITY_TYPE' => new IntegerField(
                'ENTITY_TYPE',
                [
                    'required' => true,
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ENTITY_TYPE_FIELD'),
                ]
            ),
            'ENTITY_ID' => new IntegerField(
                'ENTITY_ID',
                [
                    'required' => true,
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ENTITY_ID_FIELD'),
                ]
            ),
            'ENTITY_VALUE' => new StringField(
                'ENTITY_VALUE',
                [
                    'validation' => function()
                    {
                        return[
                            new LengthValidator(null, 255),
                        ];
                    },
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ENTITY_VALUE_FIELD'),
                ]
            ),
            'ENTITY_DATA' => new TextField(
                'ENTITY_DATA',
                [
                    'required' => true,
                    'title' => Loc::getMessage('ORDER_DISCOUNT_DATA_ENTITY_ENTITY_DATA_FIELD'),
                ]
            ),
            'ORDER' => new Reference(
                'ORDER',
                '\Bitrix\Sale\SaleOrder',
                ['=this.ORDER_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }
}