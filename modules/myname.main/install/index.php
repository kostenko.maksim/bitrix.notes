<?php
class myname_main extends CModule
{
    public $MODULE_ID           = 'myname.main';
    public $MODULE_NAME         = 'Myname: основной модуль';
    public $MODULE_DESCRIPTION  = 'Общий функционал для сайта';
    public $PARTNER_NAME        = 'Myname';
    public $PARTNER_URI         = 'https://Myname.ru';

    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    /**
     * Основные параметры модуля
     */
    public function __construct()
    {
        include __DIR__ . '/version.php';
        /** @var array $arModuleVersion */

        $this->MODULE_VERSION       = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE  = $arModuleVersion['VERSION_DATE'];
    }

    /**
     * Установка модуля
     */
    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
    }

    /**
     * Удаление модуля
     */
    function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);
    }
}
